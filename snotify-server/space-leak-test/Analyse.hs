{-# LANGUAGE OverloadedStrings #-}

module Main where

import Snotify.Import as X

import qualified Data.Text as T
import qualified Database.Persist.Sqlite as DB
import Lens.Micro
import Snotify.Model as X

import Snotify.Actor.EnsureAnalyse
import Snotify.Actor.Monad
import Snotify.Actor.Queue

main :: IO ()
main =
  withActor $ \aenv -> do
    runStderrLoggingT $ runActor aenv $ do
      repoId <- setupTestRepoInDb
      ensureAnalyse repoId

withActor :: (ActorEnv -> IO a) -> IO a
withActor f = do
  cacheDir <- resolveDir' "cache"
  -- withSystemTempDir "snotify-cache" $ \cacheDir ->
  withSystemTempFile "snotify-test" $ \dbFile _ ->
    runNoLoggingT $
    DB.withSqlitePoolInfo
      (DB.mkSqliteConnectionInfo (T.pack $ toFilePath dbFile) & DB.walEnabled .~ False &
       DB.fkEnabled .~ False)
      1 $ \pool ->
      liftIO $ do
        actionQueue <- newActionQueue 100
        void $ DB.runSqlPool (DB.runMigrationSilent migrateAll) pool
        f
          ActorEnv
            { actorEnvConnectionPool = pool
            , actorEnvCacheDir = cacheDir
            , actorEnvActionQueue = actionQueue
            , actorEnvGitExecutable = Nothing
            }

setupTestRepoInDb :: Actor RepositoryId
setupTestRepoInDb =
  actorDB $ do
    uid <- createDummyUser "testuser"
    DB.insert
      Repository
        { repositoryUser = uid
        , repositoryUrl = "https://github.com/CSVdB/pinky"
        , repositoryEnabled = True
        }
