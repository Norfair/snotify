{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Snotify
  ( snotifyServer
  ) where

import Snotify.Handler.Import

import qualified Data.Text as T

import Lens.Micro

import qualified Database.Persist.Sqlite as DB

import qualified Network.HTTP.Client as Http
import qualified Network.HTTP.Client.TLS as Http

import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.Wai.Middleware.RequestLogger as Wai

import Snotify.Application ()
import Snotify.OptParse

import Snotify.Actor
import Snotify.Actor.Monad
import Snotify.Actor.Queue

import Snotify.Looper
import Snotify.Looper.Monad

snotifyServer :: IO ()
snotifyServer = do
  Instructions disp Settings <- getInstructions
  case disp of
    DispatchServe serveSettings -> serve serveSettings

serve :: ServeSettings -> IO ()
serve sets_@ServeSettings {..} = do
  httpManager <- Http.newManager Http.tlsManagerSettings
  let runLogging = runStderrLoggingT . filterLogger (\_ ll -> ll >= serveSetLogLevel)
  runLogging $ do
    logInfoN $ T.unlines ["Running Snotify with these settings:", T.pack (ppShow sets_)]
    DB.withSqlitePoolInfo
      (DB.mkSqliteConnectionInfo (T.pack $ toFilePath serveSetDbFile) & DB.walEnabled .~ False &
       DB.fkEnabled .~ False)
      1 $ \pool ->
      liftIO $ do
        runSqlPool setupDatabase pool
        actionQueue <- newActionQueue serveSetActorQueueSize
        let app =
              App
                { appRoot = serveSetHost
                , appStatic = snotifyStatic
                , appConnPool = pool
                , appHttpManager = httpManager
                , appDummyAuth = development
                , appGoogleAnalyticsTracking = serveSetTracking
                , appGoogleSearchConsoleVerification = serveSetVerification
                , appGithubSettings = serveSetGithubSettings
                , appGitExecutable = serveSetGitExecutable
                , appCacheDir = serveSetCacheDir
                , appActionQueue = actionQueue
                }
        let defMiddles = defaultMiddlewaresNoLogging
        let extraMiddles =
              if development
                then Wai.logStdoutDev
                else Wai.logStdout
        let middle = extraMiddles . defMiddles
        plainApp <- toWaiAppPlain app
        let app_ = middle plainApp
        when development $
          runLogging $ logInfoN $ "Serving at http://localhost:" <> T.pack (show serveSetPort)
        let aenv =
              ActorEnv
                { actorEnvApp = app, actorEnvConnectionPool = pool
                , actorEnvHttpManager = httpManager
                , actorEnvCacheDir = serveSetCacheDir
                , actorEnvActionQueue = actionQueue
                , actorEnvGitExecutable = serveSetGitExecutable
                }
        let lenv = LooperEnv {looperEnvConnectionPool = pool, looperEnvActionQueue = actionQueue}
        runLogging $
          concurrently_
            (liftIO $ Warp.run serveSetPort app_)
            (concurrently_
               (runActor aenv actor)
               (runReaderT (runLooper serveSetLoopersSettings) lenv))
