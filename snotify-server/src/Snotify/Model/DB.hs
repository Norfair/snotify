{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Model.DB
  ( module Snotify.Model.DB
  , module Snotify.Model.AuthMethod
  , module Snotify.Model.SecretNotificationEmailStatus
  , module Snotify.Model.SecretStatus
  , module Snotify.Model.SecretType
  ) where

import Snotify.Import

import Database.Persist.Quasi
import Database.Persist.Sql
import Database.Persist.TH

import Snotify.Model.AuthMethod
import Snotify.Model.RepositoryAnalysisStatus
import Snotify.Model.SecretNotificationEmailStatus
import Snotify.Model.SecretStatus
import Snotify.Model.SecretType

-- You can define all of your database entities in the entities file.
-- You can find more information on persistent and how to declare entities
-- at:
-- http://www.yesodweb.com/book/persistent/
share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  $(persistFileWith lowerCaseSettings "models.txt")
