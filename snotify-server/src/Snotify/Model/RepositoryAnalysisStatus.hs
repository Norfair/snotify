{-# LANGUAGE OverloadedStrings #-}

module Snotify.Model.RepositoryAnalysisStatus where

import Snotify.Import

import Database.Persist
import Database.Persist.Sql

data RepositoryAnalysisStatus
  = Ongoing
  | Done
  deriving (Show, Eq)

instance PersistField RepositoryAnalysisStatus where
  toPersistValue am =
    PersistText $
    case am of
      Ongoing -> "ongoing"
      Done -> "done"
  fromPersistValue pv = do
    e <- fromPersistValue pv
    case e :: Text of
      "ongoing" -> Right Ongoing
      "done" -> Right Done
      _ -> Left "Unknown repository analysis status"

instance PersistFieldSql RepositoryAnalysisStatus where
  sqlType _ = SqlString
