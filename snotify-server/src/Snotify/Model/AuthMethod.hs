{-# LANGUAGE OverloadedStrings #-}

module Snotify.Model.AuthMethod where

import Snotify.Import

import Database.Persist
import Database.Persist.Sql

data AuthMethod
  = ByDummy
  | ByGithub
  | ByAdmin
  deriving (Show, Eq)

instance PersistField AuthMethod where
  toPersistValue am =
    PersistText $
    case am of
      ByDummy -> "dummy"
      ByGithub -> "github"
      ByAdmin -> "admin"
  fromPersistValue pv = do
    e <- fromPersistValue pv
    case e :: Text of
      "dummy" -> Right ByDummy
      "github" -> Right ByGithub
      "admin" -> Right ByAdmin
      _ -> Left "Unknown auth method"

instance PersistFieldSql AuthMethod where
  sqlType _ = SqlString
