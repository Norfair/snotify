{-# LANGUAGE OverloadedStrings #-}

module Snotify.Model.SecretNotificationEmailStatus where

import Snotify.Import

import Database.Persist
import Database.Persist.Sql

data SecretNotificationEmailStatus
  = EmailUnsent
  | EmailSent
  | EmailError
  deriving (Show, Eq)

instance PersistField SecretNotificationEmailStatus where
  toPersistValue am =
    PersistText $
    case am of
      EmailUnsent -> "unsent"
      EmailSent -> "sent"
      EmailError -> "error"
  fromPersistValue pv = do
    e <- fromPersistValue pv
    case e :: Text of
      "unsent" -> Right EmailUnsent
      "sent" -> Right EmailSent
      "error" -> Right EmailError
      _ -> Left "Unknown secret notification email status"

instance PersistFieldSql SecretNotificationEmailStatus where
  sqlType _ = SqlString
