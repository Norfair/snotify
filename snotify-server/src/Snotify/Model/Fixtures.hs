{-# LANGUAGE OverloadedStrings #-}

module Snotify.Model.Fixtures where

import Snotify.Import

import Snotify.Model.DB

import Database.Persist.Sql

runFixtures :: MonadIO m => ReaderT SqlBackend m ()
runFixtures =
  ensureAdminUser
    "admin"
    "sha256|14|JaX7tRhQ3jPNBQRHNYG5VA==|QxxYiqcrXN8iiDuRDy15Haf7ed5IFTVdBGQqQ+zjOPo="

createDummyUser :: MonadIO m => Text -> ReaderT SqlBackend m UserId
createDummyUser ident = do
  uid <- insert User {userAuthMethod = ByDummy}
  void $ insert $ DummyUser {dummyUserUser = uid, dummyUserIdent = ident}
  pure uid

ensureAdminUser :: MonadIO m => Text -> Text -> ReaderT SqlBackend m ()
ensureAdminUser un hashedPass = do
    ma <-getBy $ UniqueAdmin un
    case ma of
        Nothing -> void $ createAdminUser un hashedPass
        Just _ -> pure ()

createAdminUser :: MonadIO m => Text -> Text -> ReaderT SqlBackend m UserId
createAdminUser ident hashedPass = do
  uid <- insert User {userAuthMethod = ByAdmin}
  void $
    insert $ AdminUser {adminUserUser = uid, adminUserPass = hashedPass, adminUserIdent = ident}
  pure uid
