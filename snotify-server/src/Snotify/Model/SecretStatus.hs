{-# LANGUAGE OverloadedStrings #-}

module Snotify.Model.SecretStatus where

import Snotify.Import

import Database.Persist
import Database.Persist.Sql

data SecretStatus
  = Solved
  | FalsePositive
  deriving (Show, Eq)

instance PersistField SecretStatus where
  toPersistValue am =
    PersistText $
    case am of
      Solved -> "solved"
      FalsePositive -> "false positive"
  fromPersistValue pv = do
    e <- fromPersistValue pv
    case e :: Text of
      "solved" -> Right Solved
      "false positive" -> Right FalsePositive
      _ -> Left "Unknown secret status"

instance PersistFieldSql SecretStatus where
  sqlType _ = SqlString

prettySecretStatus :: SecretStatus -> Text
prettySecretStatus ss = case ss of
  Solved -> "Solved"
  FalsePositive -> "False positive"
