{-# LANGUAGE OverloadedStrings #-}

module Snotify.Model.SecretType where

import Snotify.Import

import Database.Persist
import Database.Persist.Sql

data SecretType
  = SSHSecret
  | StripeSecret
  deriving (Show, Eq)

instance PersistField SecretType where
  toPersistValue am =
    PersistText $
    case am of
      SSHSecret -> "ssh"
      StripeSecret -> "stripe"
  fromPersistValue pv = do
    e <- fromPersistValue pv
    case e :: Text of
      "ssh" -> Right SSHSecret
      "stripe" -> Right StripeSecret
      _ -> Left "Unknown key type"

instance PersistFieldSql SecretType where
  sqlType _ = SqlString
