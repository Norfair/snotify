module Snotify.Actor where

import Snotify.Import

import Snotify.Actor.Action
import Snotify.Actor.Monad

import Snotify.Actor.Analyse
import Snotify.Actor.Clone
import Snotify.Actor.GithubImport
import Snotify.Actor.EnsureAnalyse
import Snotify.Actor.Fetch
import Snotify.Actor.Notify

actor :: Actor ()
actor = do
  queue <- asks actorEnvActionQueue
  let loop = do
        a <- atomically $ readTBQueue queue
        case a of
          ActionClone rid -> clone rid
          ActionFetch rid -> fetch rid
          ActionAnalyse rid -> analyse rid
          ActionEnsureAnalyse rid -> ensureAnalyse rid
          ActionNotify sid -> notify sid
          ActionGithubImport guid -> githubImport guid
        loop
  loop
