{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module Snotify.Foundation where

import Snotify.Import.NoFoundation

import qualified Data.Text as T
import Database.Persist.Sql (ConnectionPool, runSqlPool)
import Text.Hamlet (hamletFile)

import Network.HTTP.Client as Http
import Network.OAuth.OAuth2 (AccessToken(..))

import Yesod.Auth.Dummy
import Yesod.Auth.Message
import Yesod.Auth.OAuth2
import Yesod.Auth.OAuth2.GitHub
import Yesod.Core.Types
import Yesod.EmbeddedStatic

import Snotify.Actor.Action
import Snotify.Actor.Queue

data App =
  App
    { appRoot :: Maybe Text
    , appStatic :: EmbeddedStatic
    , appConnPool :: ConnectionPool -- ^ Database connection pool.
    , appHttpManager :: Http.Manager
    , appDummyAuth :: Bool
    , appGoogleAnalyticsTracking :: Maybe Text
    , appGoogleSearchConsoleVerification :: Maybe Text
    , appGithubSettings :: Maybe GithubSettings
    , appGitExecutable :: Maybe FilePath
    , appCacheDir :: Path Abs Dir
    , appActionQueue :: ActionQueue
    }

class HasCacheDir a where
  askCacheDir :: a -> Path Abs Dir

instance HasCacheDir App where
  askCacheDir = appCacheDir

instance HasCacheDir (HandlerData App App) where
  askCacheDir = askCacheDir . rheSite . handlerEnv

-- This function also generates the following type synonyms:
-- type Handler = HandlerT App IO
-- type Widget = WidgetT App IO ()
mkYesodData "App" $(parseRoutesFile "routes")

-- | A convenient synonym for creating forms.
type Form x = Html -> MForm (HandlerFor App) (FormResult x, Widget)

-- | A convenient synonym for database access functions.
type DB a
   = forall (m :: * -> *). (MonadIO m) =>
                             ReaderT SqlBackend m a

-- Please see the documentation for the Yesod typeclass. There are a number
-- of settings which can be configured by overriding methods here.
instance Yesod App
    -- Controls the base of generated URLs. For more information on modifying,
    -- see: https://github.com/yesodweb/yesod/wiki/Overriding-approot
                                                                      where
  approot :: Approot App
  approot =
    ApprootRequest $ \app req ->
      case appRoot app of
        Nothing -> getApprootText guessApproot app req
        Just root -> root
    -- Store session data on the client in encrypted cookies,
    -- default session idle timeout is 120 minutes
  makeSessionBackend :: App -> IO (Maybe SessionBackend)
  makeSessionBackend _ =
    Just <$>
    defaultClientSessionBackend
      120 -- timeout in minutes
      "client_session_key.aes"
    -- Yesod Middleware allows you to run code before and after each handler function.
    -- The defaultYesodMiddleware adds the response header "Vary: Accept, Accept-Language" and performs authorization checks.
    -- Some users may also want to add the defaultCsrfMiddleware, which:
    --   a) Sets a cookie with a CSRF token in it.
    --   b) Validates that incoming write requests include that token in either a header or POST parameter.
    -- To add it, chain it together with the defaultMiddleware: yesodMiddleware = defaultYesodMiddleware . defaultCsrfMiddleware
    -- For details, see the CSRF documentation in the Yesod.Core.Handler module of the yesod-core package.
  yesodMiddleware :: ToTypedContent res => Handler res -> Handler res
  yesodMiddleware = defaultYesodMiddleware
  defaultLayout :: Widget -> Handler Html
  defaultLayout widget = do
    app <- getYesod
    mmsg <- getMessage
    maid <- maybeAuth
    let navbar = $(widgetFile "navbar")
    -- you to use normal widget features in default-layout.
    pc <-
      widgetToPageContent $ do
        addStylesheet $ StaticR css_bootstrap_css
        $(widgetFile "default-layout")
    withUrlRenderer $(hamletFile "templates/default-layout-wrapper.hamlet")
    -- The page to be redirected to when authentication is required.
  authRoute :: App -> Maybe (Route App)
  authRoute _ = Just $ AuthR LoginR
  isAuthorized ::
       Route App -- ^ The route the user is visiting.
    -> Bool -- ^ Whether or not this is a "write" request.
    -> Handler AuthResult
    -- Routes not requiring authentication.
  isAuthorized (AuthR _) _ = return Authorized
  isAuthorized HomeR _ = return Authorized
  isAuthorized DocsR _ = return Authorized
  isAuthorized RobotsR _ = return Authorized
  isAuthorized FaviconR _ = return Authorized
  isAuthorized (StaticR _) _ = return Authorized
    -- the profile route requires that the user is authenticated, so we
    -- delegate to that function
  isAuthorized OverviewR _ = isAuthenticated
  isAuthorized ProfileR _ = isAuthenticated
  isAuthorized ProfileGithubImportR _ = isAuthenticated
  isAuthorized (RepositoryR _) _ = isAuthenticated
  isAuthorized (RepositoryEnableR _) _ = isAuthenticated
  isAuthorized (RepositoryDisableR _) _ = isAuthenticated
  isAuthorized (RepositoryCloneR _) _ = isAuthenticated
  isAuthorized (RepositoryFetchR _) _ = isAuthenticated
  isAuthorized (RepositoryAnalyseR _) _ = isAuthenticated
  isAuthorized (SecretSolvedR _) _ = isAuthenticated
  isAuthorized (SecretFalsePositiveR _) _ = isAuthenticated
  isAuthorized (AdminR AdminLoginR) _ = pure Authorized
  isAuthorized (AdminR _) _ = isAdmin

-- How to run database actions.
instance YesodPersist App where
  type YesodPersistBackend App = SqlBackend
  runDB :: SqlPersistT Handler a -> Handler a
  runDB action = do
    app <- getYesod
    runSqlPool action $ appConnPool app

instance YesodPersistRunner App where
  getDBRunner :: Handler (DBRunner App, Handler ())
  getDBRunner = defaultGetDBRunner appConnPool

instance YesodAuth App where
  type AuthId App = UserId
    -- Where to send a user after successful login
  loginDest :: App -> Route App
  loginDest _ = OverviewR
    -- Where to send a user after logout
  logoutDest :: App -> Route App
  logoutDest _ = HomeR
  authenticate :: (MonadHandler m, HandlerSite m ~ App) => Creds App -> m (AuthenticationResult App)
  authenticate creds = do
    let ident = credsIdent creds
    case credsPlugin creds of
      "dummy" ->
        liftHandler $
        runDB $ do
          x <- getBy $ UniqueDummyUser ident
          case x of
            Just (Entity _ du) -> return $ Authenticated $ dummyUserUser du
            Nothing -> do
              uid <- createDummyUser ident
              pure $ Authenticated uid
      "github" ->
        liftHandler $ do
          x <- runDB $ getBy $ UniqueGithubUser ident
          case x of
            Just (Entity _ gu) -> return $ Authenticated $ githubUserUser gu
            Nothing -> do
              uid <- createGithubUser creds
              pure $ Authenticated uid
      "admin" ->
        liftHandler $ do
          x <- runDB $ getBy $ UniqueAdmin ident
          case x of
            Just (Entity _ au) -> return $ Authenticated $ adminUserUser au
            Nothing -> return $ UserError $ IdentifierNotFound ident
      pluginName -> error $ "Unknown authentication plugin: " <> T.unpack pluginName
    -- You can add other plugins like Google Email, email or OAuth here
  authPlugins :: App -> [AuthPlugin App]
  authPlugins app =
    [oauthPlugin gs | gs <- maybeToList (appGithubSettings app)] ++ [authDummy | appDummyAuth app]

oauthPlugin :: GithubSettings -> AuthPlugin App
oauthPlugin gs =
  (oauth2GitHub (githubSettingsClientId gs) (githubSettingsClientSecret gs))
    { apLogin =
        \toParent ->
          [whamlet|
            <div>
              <p .text-center>
                Snotify uses GitHub for authentication and only asks for read-only access to your email address.
              <p .text-center>
                Snotify sends notification emails when it finds secrets.

            <div .row .d-flex .justify-content-center>
              <p>
                <a .btn .btn-primary href=@{toParent $ oauth2Url "github"}>
                  Sign in with GitHub

            <div>
              <p .text-center .text-muted>
                Snotify does not send product update emails by default, but you can opt into those later.
          |]
    }

createGithubUser :: Creds App -> Handler UserId
createGithubUser creds = do
  let ident = credsIdent creds
  let mToken = atoken <$> getAccessToken creds
  (uid, guid) <-
    runDB $ do
      uid <- insert User {userAuthMethod = ByGithub}
      guid <-
        insert
          GithubUser {githubUserUser = uid, githubUserIdent = ident, githubUserAccessToken = mToken}
      pure (uid, guid)
  scheduleAction $ ActionGithubImport guid
  pure uid

-- | Access function to determine if a user is logged in.
isAuthenticated :: Handler AuthResult
isAuthenticated = do
  muid <- maybeAuthId
  return $
    case muid of
      Nothing -> Unauthorized "You must login to access this page."
      Just _ -> Authorized

isAdmin :: Handler AuthResult
isAdmin = do
  muid <- maybeAuthId
  case muid of
    Nothing -> pure $ Unauthorized "You must be logged in as an admin to access this page."
    Just uid -> do
      ma <- runDB $ selectFirst [AdminUserUser ==. uid] []
      case ma of
        Nothing -> pure $ Unauthorized "You must be an admin to access this page."
        Just _ -> pure Authorized

instance YesodAuthPersist App

-- This instance is required to use forms. You can modify renderMessage to
-- achieve customized and internationalized form validation messages.
instance RenderMessage App FormMessage where
  renderMessage :: App -> [Lang] -> FormMessage -> Text
  renderMessage _ _ = defaultFormMessage

-- Useful when writing code that is re-usable outside of the Handler context.
-- An example is background jobs that send email.
-- This can also be useful for writing code that works across multiple Yesod applications.
instance HasHttpManager App where
  getHttpManager :: App -> Manager
  getHttpManager = appHttpManager

scheduleAction :: Action -> Handler ()
scheduleAction a = do
  queue <- getsYesod appActionQueue
  res <- liftIO $ enqueueAction queue a
  case res of
    Enqueued -> pure ()
    QueueWasFull ->
      addMessage "Queue full" "Unable to schedule action, queue is already full, try again later."
