module Snotify.Utils.Repository where

import Snotify.Handler.Import

import qualified System.Directory as System

import Text.Printf

repositorySpaceUsage :: (HasCacheDir a, MonadReader a m, MonadIO m) => Repository -> m Bytes
repositorySpaceUsage repo = do
  dir <- repoCloneDir repo
  liftIO $ getDirectorySize dir

getDirectorySize :: Path Abs Dir -> IO Bytes
getDirectorySize =
  fmap (fromMaybe mempty) .
  forgivingAbsence .
  walkDirAccum
    Nothing
    (\dir _ files -> do
       db <- getRawDirectorySize dir
       fbs <- mapM getFileSize files
       pure $ sumBytes $ db : fbs)

getRawDirectorySize :: Path Abs Dir -> IO Bytes
getRawDirectorySize dir =
  fromMaybe mempty <$> forgivingAbsence (Bytes <$> System.getFileSize (fromAbsDir dir))

getFileSize :: Path Abs File -> IO Bytes
getFileSize f = fromMaybe mempty <$> forgivingAbsence (Bytes <$> System.getFileSize (fromAbsFile f))

newtype Bytes =
  Bytes
    { unBytes :: Integer
    }
  deriving (Show, Eq, Ord)

instance Semigroup Bytes where
  Bytes b1 <> Bytes b2 = Bytes $ b1 + b2

instance Monoid Bytes where
  mempty = Bytes 0
  mappend = (<>)

sumBytes :: [Bytes] -> Bytes
sumBytes = Bytes . sum . map unBytes

prettyBytes :: Bytes -> Text
prettyBytes (Bytes i) =
  fromString $
  case bytes of
    _
      | floor gibiBytes > (0 :: Integer) -> show (ceiling gibiBytes :: Integer) <> "GiB"
      | floor mebiBytes > (0 :: Integer) -> show (ceiling mebiBytes :: Integer) <> "MiB"
      | floor kibiBytes > (0 :: Integer) -> show (ceiling kibiBytes :: Integer) <> "KiB"
    _ -> show bytes <> "B"
  where
    bytes = fromIntegral i :: Double
    kibiBytes = bytes / 1024
    mebiBytes = kibiBytes / 1024
    gibiBytes = mebiBytes / 1024

repoCloned :: (HasCacheDir a, MonadReader a m, MonadIO m) => Repository -> m Bool
repoCloned = repoCloneDir >=> doesDirExist

repoCloneDir :: (HasCacheDir a, MonadReader a m, MonadIO m) => Repository -> m (Path Abs Dir)
repoCloneDir repo = do
  cacheDir <- asks askCacheDir
  resolveDir cacheDir $ printf "%016x" $ hash $ repositoryUrl repo
