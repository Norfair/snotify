module Snotify.Utils.Git where

import Snotify.Import

import System.Process

runGitRawIn :: Maybe FilePath -> Maybe FilePath -> [String] -> IO ()
runGitRawIn mge mcwd args = do
  let cp = (proc (fromMaybe "git" mge) args) {cwd = mcwd}
  (_, _, _, ph) <- createProcess cp
  void $ waitForProcess ph
