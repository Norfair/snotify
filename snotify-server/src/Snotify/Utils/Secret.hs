{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

module Snotify.Utils.Secret
  ( prettySecretType
  , prettySecret
  ) where

import Snotify.Handler.Import

import qualified Data.ByteString.Char8 as SB8

prettySecretType :: SecretType -> Text
prettySecretType st =
  case st of
    SSHSecret -> "SSH Key"
    StripeSecret -> "Stripe Key"

prettySecret :: ByteString -> Widget
prettySecret bs = [whamlet| #{take 16 $ SB8.unpack bs} ... |]
