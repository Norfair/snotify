{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Snotify.Analysis.Git.Git where

import Snotify.Actor.Import

import Control.Monad.Catch (MonadCatch, MonadMask, MonadThrow)
import qualified Data.ByteString as SB
import qualified Data.ByteString.Lazy as LB
import qualified Data.ByteString.Lazy.Internal as LB
import Data.HashSet (HashSet)
import qualified Data.HashSet as HS
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE

import qualified Filesystem.Path.CurrentOS as SystemPath

import Data.Git as Git
import Data.Git.Monad as GitMonad
import Data.Git.Ref (SHA1, toHexString)
import Data.Git.Storage.Object
import Data.Git.Types as Git

import Snotify.Analysis.SSH
import Snotify.Analysis.Stripe

import Snotify.Utils.Repository

analyse :: RepositoryId -> Actor ()
analyse rid = do
  repository <- actorDB $ get404 rid
  dir <- repoCloneDir repository
  pool <- asks actorEnvConnectionPool
  now <- liftIO getCurrentTime
  let url = repositoryUrl repository
  logDebugNS "Actor: Analyse" $ "analysing " <> url
  aid <-
    actorDB $
    insert
      RepositoryAnalysis
        { repositoryAnalysisRepo = rid
        , repositoryAnalysisTimestamp = now
        , repositoryAnalysisStatus = Ongoing
        }
  logFunc <- askLoggerIO
  ref <- newIORef HS.empty
  liftIO $
    Git.withRepo (SystemPath.fromText $ T.pack $ fromAbsDir dir) $ \repo ->
      let aenv =
            AnalysisEnv
              { analysisEnvRepositoryId = rid
              , analysisEnvAnalysisId = aid
              , analysisEnvConnectionPool = pool
              , analysisEnvObjsAnalysed = ref
              , analysisEnvGitRepo = repo
              }
       in runLoggingT (runReaderT (unAnalysisM analyseRepo) aenv) logFunc
  actorDB $ update aid [RepositoryAnalysisStatus =. Done]

newtype AnalysisM a =
  AnalysisM
    { unAnalysisM :: ReaderT AnalysisEnv (LoggingT IO) a
    }
  deriving ( Functor
           , Applicative
           , Monad
           , MonadIO
           , MonadThrow
           , MonadCatch
           , MonadMask
           , MonadUnliftIO
           , MonadLogger
           , MonadReader AnalysisEnv
           )

instance GitMonad AnalysisM where
  getGit = AnalysisM $ asks analysisEnvGitRepo
  liftGit = liftIO

data AnalysisEnv =
  AnalysisEnv
    { analysisEnvRepositoryId :: RepositoryId
    , analysisEnvAnalysisId :: RepositoryAnalysisId
    , analysisEnvConnectionPool :: ConnectionPool
    , analysisEnvObjsAnalysed :: IORef (HashSet Text)
    , analysisEnvGitRepo :: Git SHA1
    }

class MonadIO m =>
      AnalysisMonad m
  where
  getAnalysisEnv :: m AnalysisEnv

instance AnalysisMonad AnalysisM where
  getAnalysisEnv = AnalysisM ask

instance AnalysisMonad m => AnalysisMonad (ReaderT r m) where
  getAnalysisEnv = lift getAnalysisEnv

analysisDB :: (MonadUnliftIO m, AnalysisMonad m) => SqlPersistT m a -> m a
analysisDB action = do
  pool <- analysisEnvConnectionPool <$> getAnalysisEnv
  runSqlPool action pool

analyseRepo :: AnalysisM ()
analyseRepo = do
  bl <- GitMonad.branchList
  debug "ANALYSIS START"
  forM_ bl startAnalysis
  debug "ANALYSIS DONE"
  -- mRef <- headResolv
  -- forM_ mRef (analyseObject Nothing)

debug :: Text -> AnalysisM ()
debug = logDebugNS "ANALYSE"

textRef :: Ref SHA1 -> Text
textRef = T.pack . toHexString

type CommitRef = Ref SHA1

type BlobRef = Ref SHA1

startAnalysis :: RefName -> AnalysisM ()
startAnalysis rn = do
  mRef <- resolve rn
  forM_ mRef analyseTopLevel

analyseTopLevel :: Ref SHA1 -> AnalysisM ()
analyseTopLevel ref = do
  repo <- getGit
  mObject <- liftGit $ getObject repo ref True
  forM_ mObject $ \o ->
    case o of
      ObjCommit commit -> analyseCommit ref commit
      ObjTag tag -> analyseTag tag
      _ -> do
        logErrorNS "ANALYSE" $ "SHOULD NOT HAVE HAPPENED: " <> T.pack (show o)

analyseTag :: Tag SHA1 -> AnalysisM ()
analyseTag tag = do
  debug $ "ANALYSING TAG: " <> textRef (tagRef tag)
  analyseTopLevel $ tagRef tag

analyseCommit :: CommitRef -> Commit SHA1 -> AnalysisM ()
analyseCommit ref Commit {..} = do
  let commitHash = T.pack $ toHexString ref
  rid <- analysisEnvRepositoryId <$> getAnalysisEnv
  mAnalysed <- analysisDB $ getBy $ UniqueRepoCommitAnalysed rid commitHash
  case mAnalysed of
    Just _ -> pure ()
    Nothing -> do
      mapM_ analyseTopLevel commitParents
      debug $ "ANALYSING COMMIT: " <> textRef ref
      repo <- getGit
      mTree <- liftIO $ resolveTreeish repo commitTreeish
      forM_ mTree $ analyseTree ref []
      now <- liftIO getCurrentTime
      analysisDB $
        insert_
          CommitAnalysed
            { commitAnalysedRepo = rid
            , commitAnalysedCommit = commitHash
            , commitAnalysedTimestamp = now
            }

pathText :: EntPath -> Text
pathText = TE.decodeUtf8 . SB.intercalate "/" . map getEntNameBytes

analyseTree :: CommitRef -> EntPath -> Tree SHA1 -> AnalysisM ()
analyseTree cr path tree = do
  debug $ "ANALYSING TREE: " <> pathText path
  mapM_
    (\(_, name, ref) ->
       let n = path `entPathAppend` name
        in analyseTreeEnt cr n ref) $
    treeGetEnts tree

analyseTreeEnt :: CommitRef -> EntPath -> Ref SHA1 -> AnalysisM ()
analyseTreeEnt cr path ref = do
  debug $ "ANALYSING TREE ENTITY: " <> textRef ref
  repo <- getGit
  mObject <- liftGit $ getObject repo ref True
  forM_ mObject $ \o ->
    case o of
      ObjCommit commit -> analyseCommit ref commit
      ObjTree tree -> analyseTree cr path tree
      ObjBlob blob -> analyseBlob ref cr path blob
      _ -> do
        logErrorNS "ANALYSE" $ "SHOULD NOT HAVE HAPPENED: " <> T.pack (show o)

analyseBlob :: BlobRef -> CommitRef -> EntPath -> Blob SHA1 -> AnalysisM ()
analyseBlob br cr path blob = do
  analysed <- blobAnalysed br
  if analysed
    then pure ()
    else do
      debug $ "ANALYSING BLOB at " <> textRef cr <> ": " <> pathText path
      let blobContents = blobGetContent blob
      if lbSizeGreaterThan sizeThreshold blobContents
        then logWarnNS "ANALYSE" $ "Ignoring blob because it is too big"
        else do
          let blobBS = LB.toStrict blobContents
          rid <- analysisEnvRepositoryId <$> getAnalysisEnv
          case findSSHKey blobBS of
            Nothing -> pure ()
            Just k ->
              void $
              analysisDB $
              insertUnique
                SecretFound
                  { secretFoundRepo = rid
                  , secretFoundPath = pathText path
                  , secretFoundCommit = textRef cr
                  , secretFoundType = SSHSecret
                  , secretFoundSecret = k
                  , secretFoundStatus = Nothing
                  }
          case findStripeKey blobBS of
            Nothing -> pure ()
            Just k ->
              void $
              analysisDB $
              insertUnique
                SecretFound
                  { secretFoundRepo = rid
                  , secretFoundPath = pathText path
                  , secretFoundCommit = textRef cr
                  , secretFoundType = StripeSecret
                  , secretFoundSecret = k
                  , secretFoundStatus = Nothing
                  }
      insertAnalysed br

lbSizeGreaterThan :: Int -> LB.ByteString -> Bool
lbSizeGreaterThan i = go i
  where
    go :: Int -> LB.ByteString -> Bool
    go s LB.Empty = s < 0
    go s (LB.Chunk c cs) =
      let s' = s - SB.length c
       in if s' <= 0
            then True
            else go s' cs

sizeThreshold :: Int
sizeThreshold = 1000000 -- One megabyte

blobAnalysed :: Ref SHA1 -> AnalysisM Bool
blobAnalysed ref = do
  os <- asks analysisEnvObjsAnalysed
  hs <- readIORef os
  pure $ HS.member (textRef ref) hs

insertAnalysed :: Ref SHA1 -> AnalysisM ()
insertAnalysed ref = do
  os <- asks analysisEnvObjsAnalysed
  modifyIORef os (HS.insert $ textRef ref)
