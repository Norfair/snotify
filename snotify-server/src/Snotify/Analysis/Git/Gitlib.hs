{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Snotify.Analysis.Git.Gitlib where

import Snotify.Import.NoFoundation

import qualified Data.Text.Encoding as TE

import Git
import Git.Libgit2 (lgFactory)

import Snotify.Analysis.Monad
import Snotify.Analysis.SSH
import Snotify.Analysis.Stripe

doAnalysis :: Path Abs Dir -> AnalysisM ()
doAnalysis p =
  Git.withRepository'
    lgFactory
    (defaultRepositoryOptions {repoPath = fromAbsDir p, repoAutoCreate = False, repoIsBare = True}) $ do
    mOid <- resolveReference "HEAD"
    forM_ mOid $ \oid -> do
      obj <- lookupObject oid
      analyseObj obj

analyseRepo :: GitM r m => m ()
analyseRepo = do
  bl <- listReferences
  debug "ANALYSIS START"
  forM_ bl analyseRef
  debug "ANALYSIS DONE"

analyseRef :: GitM r m => RefName -> m ()
analyseRef ref = do
  debug $ "ANALYSING REFERENCE: " <> ref
  moid <- resolveReference ref
  forM_ moid analyseObjId

analyseObjId ::
     forall r m. GitM r m
  => Oid r
  -> m ()
analyseObjId i = do
  obj <- lookupObject i :: m (Object r m)
  analyseObj obj

analyseObj :: GitM r m => Object r m -> m ()
analyseObj o =
  case o of
    TagObj tag -> analyseTag tag
    CommitObj commit -> analyseCommit commit
    _ -> do
      logErrorNS "ANALYSE" $ "SHOULD NOT HAVE HAPPENED"

analyseTag :: GitM r m => Tag r -> m ()
analyseTag tag = do
  debug $ "ANALYSING TAG: " <> renderObjOid (tagOid tag)
  analyseCommitId $ tagCommit tag

analyseCommitId :: GitM r m => CommitOid r -> m ()
analyseCommitId cid = do
  commit <- lookupCommit cid
  analyseCommit commit

analyseCommit :: GitM r m => Commit r -> m ()
analyseCommit commit = do
  let commitHash = renderObjOid $ commitOid commit
  rid <- analysisEnvRepositoryId <$> getAnalysisEnv
  mAnalysed <- analysisDB $ getBy $ UniqueRepoCommitAnalysed rid commitHash
  case mAnalysed of
    Just _ -> pure ()
    Nothing -> do
      mapM_ analyseCommitId $ commitParents commit
      debug $ "ANALYSING COMMIT: " <> commitHash
      analyseTreeId (commitOid commit) "" $ commitTree commit
      now <- liftIO getCurrentTime
      analysisDB $
        insert_
          CommitAnalysed
            { commitAnalysedRepo = rid
            , commitAnalysedCommit = commitHash
            , commitAnalysedTimestamp = now
            }

analyseTreeId :: GitM r m => CommitOid r -> TreeFilePath -> TreeOid r -> m ()
analyseTreeId cid path tid = do
  t <- lookupTree tid
  analyseTree cid path t

analyseTree :: GitM r m => CommitOid r -> TreeFilePath -> Tree r -> m ()
analyseTree cid path tree = do
  let commitHash = renderObjOid cid
  debug $ "ANALYSING TREE " <> commitHash <> " " <> pathText path
  entries <- listTreeEntries tree
  forM_ entries $ \(p, te) -> do
    let path' = pathAppend path p
    case te of
      BlobEntry bid _ -> analyseBlobId cid path' bid
      TreeEntry tid -> analyseTreeId cid path' tid
      CommitEntry _ -> warning "NOT ANALYSING SUBMODULE TREE ENTRY"

pathAppend :: TreeFilePath -> TreeFilePath -> TreeFilePath
pathAppend "" p = p
pathAppend p1 p2 = p1 <> "/" <> p2

analyseBlobId :: GitM r m => CommitOid r -> TreeFilePath -> BlobOid r -> m ()
analyseBlobId cid path bid = do
  analysed <- blobAnalysed $ renderObjOid bid
  if analysed
    then pure ()
    else do
      blob <- lookupBlob bid
      analyseBlob cid path blob

analyseBlob ::
     forall r m. GitM r m
  => CommitOid r
  -> TreeFilePath
  -> Blob r m
  -> m ()
analyseBlob cid path blob = do
  let bid = blobOid blob
  let commitHash = renderObjOid cid
  let blobHash = renderObjOid bid
  debug $ "ANALYSING BLOB at " <> blobHash <> ": " <> pathText path
  blobBS <- blobContentsToByteString $ blobContents blob
  rid <- analysisEnvRepositoryId <$> getAnalysisEnv
  case findSSHKey blobBS of
    Nothing -> pure ()
    Just k ->
      void $
      analysisDB $
      insertUnique
        SecretFound
          { secretFoundRepo = rid
          , secretFoundPath = pathText path
          , secretFoundCommit = commitHash
          , secretFoundType = SSHSecret
          , secretFoundSecret = k
          , secretFoundStatus = Nothing
          }
  case findStripeKey blobBS of
    Nothing -> pure ()
    Just k -> do
      void $
        analysisDB $
        insertUnique
          SecretFound
            { secretFoundRepo = rid
            , secretFoundPath = pathText path
            , secretFoundCommit = commitHash
            , secretFoundType = StripeSecret
            , secretFoundSecret = k
            , secretFoundStatus = Nothing
            }
  insertAnalysed blobHash

pathText :: TreeFilePath -> Text
pathText = TE.decodeUtf8
