{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Snotify.Analysis.Monad where

import Snotify.Actor.Import

import Control.Monad.Catch (MonadCatch, MonadMask, MonadThrow)
import qualified Data.HashSet as HS
import Data.HashSet (HashSet)

import Git
import Git.Libgit2 (HasLgRepo)

type GitM r m
   = ( MonadIO m
     , MonadMask m
     , MonadUnliftIO m
     , HasLgRepo m
     , MonadGit r m
     , MonadLogger m
     , AnalysisMonad m)

newtype AnalysisM a =
  AnalysisM
    { unAnalysisM :: ReaderT AnalysisEnv (LoggingT IO) a
    }
  deriving ( Functor
           , Applicative
           , Monad
           , MonadIO
           , MonadThrow
           , MonadCatch
           , MonadMask
           , MonadUnliftIO
           , MonadLogger
           )

data AnalysisEnv =
  AnalysisEnv
    { analysisEnvRepositoryId :: RepositoryId
    , analysisEnvAnalysisId :: RepositoryAnalysisId
    , analysisEnvConnectionPool :: ConnectionPool
    , analysisEnvObjsAnalysed :: IORef (HashSet Text)
    }

class MonadIO m =>
      AnalysisMonad m
  where
  getAnalysisEnv :: m AnalysisEnv

instance AnalysisMonad AnalysisM where
  getAnalysisEnv = AnalysisM ask

instance AnalysisMonad m => AnalysisMonad (ReaderT r m) where
  getAnalysisEnv = lift getAnalysisEnv

debug :: GitM r m => Text -> m ()
debug = logDebugNS "ANALYSE"

warning :: GitM r m => Text -> m ()
warning = logWarnNS "ANALYSE"

analysisDB :: (MonadUnliftIO m, AnalysisMonad m) => SqlPersistT m a -> m a
analysisDB action = do
  pool <- analysisEnvConnectionPool <$> getAnalysisEnv
  runSqlPool action pool

blobAnalysed ::
     forall r m. GitM r m
  => Text
  -> m Bool
blobAnalysed objHash = do
  os <- analysisEnvObjsAnalysed <$> getAnalysisEnv
  hs <- readIORef os
  pure $ HS.member objHash hs

insertAnalysed ::
     forall r m. GitM r m
  => Text
  -> m ()
insertAnalysed objHash = do
  os <- analysisEnvObjsAnalysed <$> getAnalysisEnv
  modifyIORef os (HS.insert objHash)
