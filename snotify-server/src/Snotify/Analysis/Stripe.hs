{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Snotify.Analysis.Stripe where

import Snotify.Actor.Import

import qualified Data.Attoparsec.ByteString as P
import qualified Data.Attoparsec.ByteString.Char8 as PC8
import qualified Data.ByteString as SB
import qualified Data.ByteString.Char8 as SB8

findStripeKey :: ByteString -> Maybe ByteString
findStripeKey bs =
  if quickTest bs
    then case P.parseOnly (P.try testKey <|> liveKey) bs of
           Left _ -> Nothing
           Right k -> Just k
    else Nothing

-- public test key: pk_test_AAAAAAAAAAAAAAAAAAAAAAAA
-- secret test key: sk_test_AAAAAAAAAAAAAAAAAAAAAAAA
-- public test key: pk_live_AAAAAAAAAAAAAAAAAAAAAAAA
-- secret test key: sk_live_AAAAAAAAAAAAAAAAAAAAAAAA
quickTest :: ByteString -> Bool
quickTest bs = "sk_test_" `SB.isInfixOf` bs || "sk_live_" `SB.isInfixOf` bs

testKey :: P.Parser ByteString
testKey = do
  void $ P.manyTill PC8.anyChar (P.string "sk_test_")
  key <- replicateM 24 PC8.anyChar
  pure $ "sk_test_" <> SB8.pack key

liveKey :: P.Parser ByteString
liveKey = do
  void $ P.manyTill PC8.anyChar (P.string "sk_live_")
  key <- replicateM 24 PC8.anyChar
  pure $ "sk_live_" <> SB8.pack key
