{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Snotify.Analysis.SSH where

import Snotify.Actor.Import

import qualified Data.Attoparsec.ByteString as P
import qualified Data.Attoparsec.ByteString.Char8 as PC8
import qualified Data.ByteString as SB
import qualified Data.ByteString.Char8 as SB8
import Data.Char

findSSHKey :: ByteString -> Maybe ByteString
findSSHKey bs =
  if quickTest bs
    then case P.parseOnly (P.try rsaKeyP <|> openSSHKeyP) bs of
           Left _ -> Nothing
           Right k -> Just $ normaliseSSHKey k
    else Nothing

normaliseSSHKey :: ByteString -> ByteString
normaliseSSHKey = SB8.filter (/= '\n')

quickTest :: ByteString -> Bool
quickTest bs =
  "BEGIN RSA PRIVATE KEY" `SB.isInfixOf` bs || "BEGIN OPENSSH PRIVATE KEY" `SB.isInfixOf` bs

rsaKeyP :: P.Parser ByteString
rsaKeyP = do
  void $ P.manyTill PC8.anyChar (P.string "-----BEGIN RSA PRIVATE KEY-----")
  key <- P.takeWhile (/= fromIntegral (ord '-'))
  void $ P.string "-----END RSA PRIVATE KEY-----"
  pure key

openSSHKeyP :: P.Parser ByteString
openSSHKeyP = do
  void $ P.manyTill PC8.anyChar (P.string "-----BEGIN OPENSSH PRIVATE KEY-----")
  key <- P.takeWhile (/= fromIntegral (ord '-'))
  void $ P.string "-----END OPENSSH PRIVATE KEY-----"
  pure key
