{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Snotify.OptParse
  ( getInstructions
  , Instructions(..)
  , Dispatch(..)
  , Settings(..)
  , ServeSettings(..)
  , GithubSettings(..)
  ) where

import Snotify.Import

import Data.Char as Char
import qualified System.Environment as System (getArgs, getEnvironment)

import Options.Applicative

import Snotify.Looper.Monad

import Snotify.OptParse.Types

getInstructions :: IO Instructions
getInstructions = do
  args <- getArguments
  env <- getEnvironment
  buildInstructions args env

buildInstructions :: Arguments -> Environment -> IO Instructions
buildInstructions (Arguments cmd Flags) Environment {..} = Instructions <$> disp <*> settings
  where
    disp =
      case cmd of
        CommandServe ServeFlags {..} ->
          fmap DispatchServe $ do
            serveSetDbFile <-
              resolveFile' $ fromMaybe defaultDatabaseFile $ serveFlagDbFile `mplus` envDBFile
            let serveSetLogLevel = fromMaybe LevelInfo $ serveFlagLogLevel <|> envLogLevel
            let serveSetHost = serveFlagHost <|> envHost
            let serveSetPort = fromMaybe 8000 $ serveFlagPort <|> envPort
            serveSetCacheDir <-
              resolveDir' $ fromMaybe defaultCacheDir $ serveFlagCacheDir <|> envCacheDir
            let serveSetActorQueueSize =
                  fromMaybe 1000 $ serveFlagActorQueueSize <|> envActorQueueSize
            let serveSetTracking = serveFlagTracking <|> envTracking
            let serveSetVerification = serveFlagVerification <|> envVerification
            let serveSetGithubSettings = serveFlagGithubSettings <|> envGithubSettings
            let serveSetGitExecutable = serveFlagGitExecutable <|> envGitExecutable
            let serveSetLoopersSettings =
                  deriveLoopersSettings serveFlagLoopersFlags envLoopersEnvironment
            pure ServeSettings {..}
    settings = pure Settings

defaultDatabaseFile :: FilePath
defaultDatabaseFile = "snotify.db"

defaultCacheDir :: FilePath
defaultCacheDir = "cache"

deriveLoopersSettings :: LoopersFlags -> LoopersEnvironment -> LoopersSettings
deriveLoopersSettings LoopersFlags {..} LoopersEnvironment {..} =
  LoopersSettings
    { loopersSetAnalysisSettings =
        deriveLooperSettings 0 loopersFlagAnalysisFlags loopersEnvAnalysisEnvironment
    , loopersSetEmailerSettings =
        deriveLooperSettings (minutes 1) loopersFlagEmailerFlags loopersEnvEmailerEnvironment
    }

deriveLooperSettings :: NominalDiffTime -> LooperFlags -> LooperEnvironment -> LooperSettings
deriveLooperSettings ndt LooperFlags {..} LooperEnvironment {..} =
  let looperSetEnabled = fromMaybe True $ looperFlagEnabled <|> looperEnvEnabled
      looperSetPhase = maybe ndt fromIntegral $ looperFlagPhase <|> looperEnvPhase
      looperSetPeriod = maybe (hours 1) fromIntegral $ looperFlagPeriod <|> looperEnvPeriod
   in LooperSettings {..}

getArguments :: IO Arguments
getArguments = do
  args <- System.getArgs
  let result = serveArgumentsParser args
  handleParseResult result

serveArgumentsParser :: [String] -> ParserResult Arguments
serveArgumentsParser =
  execParserPure
    ParserPrefs
      { prefMultiSuffix = ""
      , prefDisambiguate = True
      , prefShowHelpOnError = True
      , prefShowHelpOnEmpty = True
      , prefBacktrack = True
      , prefColumns = 80
      }
    argParser

argParser :: ParserInfo Arguments
argParser = info (helper <*> parseArgs) (fullDesc <> progDesc "Snotify")

parseArgs :: Parser Arguments
parseArgs = Arguments <$> parseCommand <*> parseFlags

parseCommand :: Parser Command
parseCommand = hsubparser $ mconcat [command "serve" parseCommandServe]

parseCommandServe :: ParserInfo Command
parseCommandServe = info parser modifier
  where
    parser =
      CommandServe <$>
      (ServeFlags <$>
       option
         (Just <$> str)
         (mconcat [long "db-file", metavar "FILE", value Nothing, help "The database file to use"]) <*>
       option
         (Just <$> auto)
         (mconcat [long "log-level", metavar "LOGLEVEL", value Nothing, help "the log level to use"]) <*>
       option
         (Just <$> str)
         (mconcat [long "host", metavar "HOST", value Nothing, help "the host to serve on"]) <*>
       option
         (Just <$> auto)
         (mconcat [long "port", metavar "PORT", value Nothing, help "the port to serve on"]) <*>
       option
         (Just <$> str)
         (mconcat [long "cache-dir", metavar "DIR", value Nothing, help "the cache dir to use"]) <*>
       option
         (Just <$> auto)
         (mconcat
            [ long "actor-queue-size"
            , metavar "SIZE"
            , value Nothing
            , help "the size of the actor queue"
            ]) <*>
       option
         (Just <$> str)
         (mconcat
            [ long "google-analytics-tracking"
            , metavar "TRACKING_CODE"
            , value Nothing
            , help "the google analytics tracking code to use"
            ]) <*>
       option
         (Just <$> str)
         (mconcat
            [ long "google-search-console-verification"
            , metavar "VERIFICATION_CODE"
            , value Nothing
            , help "the google search console verification code to use"
            ]) <*>
       parseGithubSettingsFlags <*>
       option
         (Just <$> str)
         (mconcat
            [ long "git-executable"
            , metavar "FILEPATH"
            , value Nothing
            , help "the path to the git executable to use"
            ]) <*>
       getLoopersFlags)
    modifier = fullDesc <> progDesc "Run the snotify server"

parseGithubSettingsFlags :: Parser (Maybe GithubSettings)
parseGithubSettingsFlags =
  ((\ma mb -> GithubSettings <$> ma <*> mb) <$>
   option
     (Just <$> str)
     (mconcat
        [long "github-client-id", metavar "CLIENT_ID", value Nothing, help "the github client id"]) <*>
   option
     (Just <$> str)
     (mconcat
        [ long "github-client-secret"
        , metavar "CLIENT_SECRET"
        , value Nothing
        , help "the github client secret"
        ]))

getLoopersFlags :: Parser LoopersFlags
getLoopersFlags = LoopersFlags <$> getLooperFlags "analysis" <*> getLooperFlags "emailer"

getLooperFlags :: String -> Parser LooperFlags
getLooperFlags name =
  LooperFlags <$>
  option
    (Just <$> auto)
    (mconcat
       [ long $ name <> "-enabled"
       , metavar "True|False"
       , value Nothing
       , help $ unwords ["enable the", name, "looper"]
       ]) <*>
  option
    (Just <$> auto)
    (mconcat
       [ long $ name <> "-phase"
       , metavar "SECONDS"
       , value Nothing
       , help $ unwords ["the phase for the", name, "looper in seconsd"]
       ]) <*>
  option
    (Just <$> auto)
    (mconcat
       [ long $ name <> "-period"
       , metavar "SECONDS"
       , value Nothing
       , help $ unwords ["the period for the", name, "looper in seconds"]
       ])

parseFlags :: Parser Flags
parseFlags = pure Flags

getEnvironment :: IO Environment
getEnvironment = do
  env <- System.getEnvironment
  let v :: IsString s => String -> Maybe s
      v k = fromString <$> lookup ("SNOTIFY_" <> k) env
  let r :: Read a => String -> Maybe a
      r k = v k >>= readMay
  let getLooperEnvironment :: String -> LooperEnvironment
      getLooperEnvironment name =
        let ln = map Char.toUpper name
            -- lv :: IsString s => String -> Maybe s
            -- lv k = v $ "LOOPER_" <> ln <> "_" <> k
            lr :: Read a => String -> Maybe a
            lr k = r $ "LOOPER_" <> ln <> "_" <> k
         in LooperEnvironment
              { looperEnvEnabled = lr "ENABLED"
              , looperEnvPhase = lr "PHASE"
              , looperEnvPeriod = lr "PERIOD"
              }
  pure
    Environment
      { envDBFile = v "DB_FILE" `mplus` v "DATABASE_FILE"
      , envLogLevel = r "LOG_LEVEL"
      , envHost = v "HOST"
      , envPort = r "PORT"
      , envCacheDir = v "CACHE_DIR"
      , envActorQueueSize = r "ACTOR_QUEUE_SIZE"
      , envTracking = v "GOOGLE_ANALYTICS_TRACKING"
      , envVerification = v "GOOGLE_SEARCH_CONSOLE_VERIFICATION"
      , envGithubSettings = GithubSettings <$> v "GITHUB_CLIENT_ID" <*> v "GITHUB_CLIENT_SECRET"
      , envGitExecutable = v "GIT_EXECUTABLE"
      , envLoopersEnvironment =
          LoopersEnvironment
            { loopersEnvAnalysisEnvironment = getLooperEnvironment "ANALYSIS"
            , loopersEnvEmailerEnvironment = getLooperEnvironment "EMAILER"
            }
      }
