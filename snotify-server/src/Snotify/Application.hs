{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Snotify.Application
  ( module Snotify.Foundation
  ) where

import Snotify.Handler.Import

import Snotify.Foundation

import Snotify.Handler.Admin
import Snotify.Handler.Common
import Snotify.Handler.Docs
import Snotify.Handler.Home
import Snotify.Handler.Overview
import Snotify.Handler.Profile
import Snotify.Handler.Repository
import Snotify.Handler.Secret

-- This line actually creates our YesodDispatch instance. It is the second half
-- of the call to mkYesodData which occurs in Foundation.hs. Please see the
-- comments there for more details.
mkYesodDispatch "App" resourcesApp
