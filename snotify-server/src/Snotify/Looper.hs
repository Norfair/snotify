{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Snotify.Looper where

import Snotify.Import

import Control.Concurrent

import Snotify.OptParse.Types

import Snotify.Looper.Monad

import Snotify.Looper.Analysis
import Snotify.Looper.Emailer

runLooper :: LoopersSettings -> Looper ()
runLooper LoopersSettings {..} =
  runLoopers
    [ mkDef "Analysis" loopersSetAnalysisSettings analysis
    , mkDef "Emailer" loopersSetEmailerSettings emailer
    ]

mkDef :: Text -> LooperSettings -> Looper () -> LooperDef
mkDef name LooperSettings {..} func =
  LooperDef
    { looperDefName = name
    , looperDefEnabled = looperSetEnabled
    , looperDefPeriod = looperSetPeriod
    , looperDefPhase = looperSetPhase
    , looperDefFunc = func
    }

runLoopers :: [LooperDef] -> Looper ()
runLoopers =
  mapConcurrently_ $ \LooperDef {..} ->
    when looperDefEnabled $ do
      waitLooper looperDefPhase
      let info = logInfoNS $ "Looper:" <> looperDefName
          warn = logWarnNS $ "Looper:" <> looperDefName
      let loop = do
            info "START"
            start <- liftIO $ getCurrentTime
            looperDefFunc
            end <- liftIO $ getCurrentTime
            info "END"
            let elapsed = diffUTCTime end start
            let nextWait = looperDefPeriod - elapsed
            when (nextWait < 0) $ warn "WARNING: looper ran over its period."
            waitLooper nextWait
            loop
      loop

waitLooper :: NominalDiffTime -> Looper ()
waitLooper ndt = liftIO $ threadDelay (round ndt * 1000 * 1000)
