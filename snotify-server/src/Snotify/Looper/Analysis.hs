module Snotify.Looper.Analysis (analysis) where

import Snotify.Looper.Import

analysis :: Looper ()
analysis = do
  repos <- looperDB $ selectList [RepositoryEnabled ==. True] [Asc RepositoryId]
  forM_ repos $ \(Entity repoid _ ) -> looperEnqueue (ActionEnsureAnalyse repoid)
