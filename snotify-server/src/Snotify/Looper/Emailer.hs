module Snotify.Looper.Emailer
  ( emailer
  ) where

import Snotify.Looper.Import

emailer :: Looper ()
emailer = do
  secretsFound <- looperDB $ selectList [SecretFoundStatus ==. Nothing] [Asc SecretFoundId]
  -- TODO: Replace with a nice join ? We may want to split it up then.
  forM_ secretsFound $ \(Entity sid _) -> do
    emails <-
      map entityVal <$>
      looperDB
        (selectList
           [SecretNotificationEmailSecret ==. sid]
           [ Asc SecretNotificationEmailStatus
           , Asc SecretNotificationEmailTimestamp
           , Asc SecretNotificationEmailAttemptTimestamp
           ])
    let goOn = looperEnqueue (ActionNotify sid)
    case emails of
      [] -> goOn
      _
        | length (filter ((== EmailError) . secretNotificationEmailStatus) emails) <= maxErrors ->
          goOn
        | otherwise -> pure ()

maxErrors :: Int
maxErrors = 3
