module Snotify.Looper.Monad where

import Snotify.Import

import Database.Persist.Sql (ConnectionPool, SqlPersistT, runSqlPool)

import Snotify.Actor.Action
import Snotify.Actor.Queue

data LooperEnv =
  LooperEnv
    { looperEnvConnectionPool :: ConnectionPool
    , looperEnvActionQueue :: ActionQueue
    }

type Looper = ReaderT LooperEnv (LoggingT IO)

looperDB :: SqlPersistT Looper a -> Looper a
looperDB action = do
  pool <- asks looperEnvConnectionPool
  runSqlPool action pool

data LooperDef =
  LooperDef
    { looperDefName :: Text
    , looperDefEnabled :: Bool
    , looperDefPeriod :: NominalDiffTime
    , looperDefPhase :: NominalDiffTime
    , looperDefFunc :: Looper ()
    }

seconds :: Double -> NominalDiffTime
seconds = realToFrac

minutes :: Double -> NominalDiffTime
minutes = seconds . (* 60)

hours :: Double -> NominalDiffTime
hours = minutes . (* 60)

looperEnqueue :: Action -> Looper ()
looperEnqueue a = do
  queue <- asks looperEnvActionQueue
  liftIO $ enqueueActionBlocking queue a
