module Snotify.Actor.Queue where

import Snotify.Import

import Snotify.Actor.Action

type ActionQueue = TBQueue Action

newActionQueue :: Natural -> IO ActionQueue
newActionQueue = newTBQueueIO

data EnqueResult
  = Enqueued
  | QueueWasFull

enqueueAction :: ActionQueue -> Action -> IO EnqueResult
enqueueAction queue a =
  atomically $ do
    full <- isFullTBQueue queue
    if full
      then pure QueueWasFull
      else writeTBQueue queue a >> pure Enqueued

enqueueActionBlocking :: ActionQueue -> Action -> IO ()
enqueueActionBlocking queue a = atomically $ do writeTBQueue queue a
