module Snotify.Actor.Action where

import Snotify.Import.NoFoundation

data Action
  = ActionClone RepositoryId
  | ActionFetch RepositoryId
  | ActionAnalyse RepositoryId
  | ActionEnsureAnalyse RepositoryId
  | ActionNotify SecretFoundId
  | ActionGithubImport GithubUserId
  deriving (Show, Eq)
