{-# LANGUAGE OverloadedStrings #-}

module Snotify.Actor.GithubImport where

import Snotify.Actor.Import

import qualified Data.Text as T
import qualified Data.Text.Encoding as TE

import qualified GitHub as GH

githubImport :: GithubUserId -> Actor ()
githubImport guid = do
  mgu <- actorDB $ get guid
  forM_ mgu $ \gu -> do
    let uid = githubUserUser gu
    forM_ (githubUserAccessToken gu) $ \token -> do
      logInfoNS "GITHUB_IMPORT" "Importing repositories"
      importRepositories uid token
      logInfoNS "GITHUB_IMPORT" "Importing email addresses"
      importEmailAddresses uid token

importRepositories :: UserId -> Text -> Actor ()
importRepositories uid token = do
  mgr <- asks actorEnvHttpManager
  errOrRepos <-
    liftIO $
    GH.executeRequestWithMgr mgr (GH.OAuth $ TE.encodeUtf8 token) $
    GH.currentUserReposR GH.RepoPublicityPublic GH.FetchAll
  case errOrRepos of
    Left err -> logErrorNS "GITHUB_IMPORT" $ T.pack $ show err
    Right repos ->
      forM_ repos $ \repo ->
        actorDB $
        insertUnique $
        Repository
          { repositoryUser = uid
          , repositoryUrl = GH.getUrl $ GH.repoHtmlUrl repo
          , repositoryEnabled = False
          }

importEmailAddresses :: UserId -> Text -> Actor ()
importEmailAddresses uid token = do
  mgr <- asks actorEnvHttpManager
  errOrEmails <-
    liftIO $
    GH.executeRequestWithMgr mgr (GH.OAuth $ TE.encodeUtf8 token) $
    GH.currentUserEmailsR GH.FetchAll
  case errOrEmails of
    Left err -> logErrorNS "GITHUB_IMPORT" $ T.pack $ show err
    Right emails ->
      forM_ emails $ \email ->
        actorDB $
        insertUnique $
        UserEmailAddress
          { userEmailAddressUser = uid
          , userEmailAddressAddress = GH.emailAddress email
          , userEmailAddressPrimary = GH.emailPrimary email
          , userEmailAddressVerified = GH.emailVerified email
          }
