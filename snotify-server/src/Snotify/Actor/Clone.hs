{-# LANGUAGE OverloadedStrings #-}

module Snotify.Actor.Clone where

import Snotify.Actor.Import

import qualified Data.Text as T

import Snotify.Utils.Repository

clone :: RepositoryId -> Actor ()
clone rid = do
  repo <- actorDB $ get404 rid
  dir <- repoCloneDir repo
  now <- liftIO getCurrentTime
  let url = repositoryUrl repo
  logDebugNS "Actor: Clone" $ "Cloning " <> url <> " in dir " <> T.pack (fromAbsDir dir)
  runActorGit ["clone", T.unpack url, fromAbsDir dir]
  runActorGit ["remote add origin", T.unpack url]
  actorDB $
    insert_ RepositoryCloning {repositoryCloningRepo = rid, repositoryCloningTimestamp = now}
