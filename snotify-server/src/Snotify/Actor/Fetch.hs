{-# LANGUAGE OverloadedStrings #-}

module Snotify.Actor.Fetch where

import Snotify.Actor.Import

import qualified Data.Text as T

import Snotify.Utils.Repository

fetch :: RepositoryId -> Actor ()
fetch rid = do
  repo <- actorDB $ get404 rid
  dir <- repoCloneDir repo
  now <- liftIO getCurrentTime
  let url = repositoryUrl repo
  logDebugNS "Actor: Fetch" $ "fetching " <> url <> " in dir " <> T.pack (fromAbsDir dir)
  runActorGitIn dir ["fetch", "--update-head-ok", T.unpack url, "master:master"]
  actorDB $
    insert_ RepositoryFetching {repositoryFetchingRepo = rid, repositoryFetchingTimestamp = now}
