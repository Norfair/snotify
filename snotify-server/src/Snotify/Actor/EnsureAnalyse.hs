module Snotify.Actor.EnsureAnalyse where

import Snotify.Actor.Import

import Snotify.Utils.Repository

import Snotify.Actor.Analyse
import Snotify.Actor.Clone
import Snotify.Actor.Fetch

ensureAnalyse :: RepositoryId -> Actor ()
ensureAnalyse rid = do
  repo <- actorDB $ get404 rid
  alreadyCloned <- repoCloned repo
  if alreadyCloned
    then fetch rid
    else clone rid
  analyse rid
