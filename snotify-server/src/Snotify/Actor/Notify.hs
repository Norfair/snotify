{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Snotify.Actor.Notify where

import Snotify.Actor.Import
import Snotify.Handler.Import

import qualified Data.List.NonEmpty as NE

import qualified Data.Text.Lazy as LT
import qualified Data.Text as T
import qualified Data.Text.Lazy.Builder as LTB

import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet
import Text.Shakespeare.Text

import Snotify.Actor.Notify.Email

notify :: SecretFoundId -> Actor ()
notify sid = do
  mSecretFound <- actorDB $ get sid
  forM_ mSecretFound $ \secretFound -> do
    let repositoryId = secretFoundRepo secretFound
    mRepository <- actorDB $ get repositoryId
    forM_ mRepository $ \repository -> do
      let userId = repositoryUser repository
      mEmailAddress <- getEmailAddress userId
      case mEmailAddress of
        Nothing ->
          logWarnNS "EMAILER" $
          "Did not send an email to user with id " <> T.pack (show $ fromSqlKey userId) <>
          " because they did not have any email addresses."
        Just emailAddress -> do
          now <- liftIO getCurrentTime
          sneid <-
            actorDB $
            insert
              SecretNotificationEmail
                { secretNotificationEmailSecret = sid
                , secretNotificationEmailAddress = emailAddress
                , secretNotificationEmailStatus = EmailUnsent
                , secretNotificationEmailTimestamp = now
                , secretNotificationEmailError = Nothing
                , secretNotificationEmailMessageId = Nothing
                , secretNotificationEmailAttemptTimestamp = Nothing
                }
          errOrId <-
            tryToSendEmail emailAddress (Entity repositoryId repository) (Entity sid secretFound)
          attempt <- liftIO getCurrentTime
          actorDB $
            case errOrId of
              Left err -> do
                logErrorNS "EMAILER" $ "Failed to send email: " <> err
                update
                  sneid
                  [ SecretNotificationEmailStatus =. EmailError
                  , SecretNotificationEmailError =. Just err
                  , SecretNotificationEmailAttemptTimestamp =. Just attempt
                  ]
              Right i ->
                update
                  sneid
                  [ SecretNotificationEmailStatus =. EmailSent
                  , SecretNotificationEmailMessageId =. Just i
                  , SecretNotificationEmailAttemptTimestamp =. Just attempt
                  ]

getEmailAddress :: UserId -> Actor (Maybe Text)
getEmailAddress uid = do
  as <- actorDB $ selectList [UserEmailAddressUser ==. uid] []
  let score :: UserEmailAddress -> Int
      score UserEmailAddress {..} =
        sum
          [ if userEmailAddressPrimary
              then 2
              else 0
          , if userEmailAddressVerified
              then 1
              else 0
          ]
  pure $
    fmap NE.head $
    NE.nonEmpty $ map userEmailAddressAddress $ sortOn (Down . score) $ map entityVal as

tryToSendEmail :: Text -> Entity Repository -> Entity SecretFound -> Actor (Either Text Text)
tryToSendEmail address re sfe = do
  y <- asks actorEnvApp
  let render = yesodRender y resolvedRoot
  sendSingleEmail
    Email
      { emailTo = address
      , emailFromName = "Snotify"
      , emailFrom = "noreply@snotify.cs-syd.eu"
      , emailSubject = "IMPORTANT: Secret found in repository"
      , emailTextContent = secretNotificationEmailTextContent re sfe render
      , emailHtmlContent = secretNotificationEmailHtmlContent re sfe render
      }

secretNotificationEmailTextContent ::
     Entity Repository -> Entity SecretFound -> Render (Route App) -> Text
secretNotificationEmailTextContent (Entity rid r) (Entity _ s) render =
  LT.toStrict $ LTB.toLazyText $ $(textFile "templates/email/secret-notification.txt") render

secretNotificationEmailHtmlContent ::
     Entity Repository -> Entity SecretFound -> Render (Route App) -> Text
secretNotificationEmailHtmlContent (Entity rid r) (Entity _ s) render =
  LT.toStrict $ renderHtml $ $(hamletFile "templates/email/secret-notification.hamlet") render
