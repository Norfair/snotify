{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Snotify.Actor.Analyse where

import Snotify.Actor.Import

import qualified Data.HashSet as HS

import Snotify.Utils.Repository

import Snotify.Analysis.Git.Gitlib
import Snotify.Analysis.Monad

analyse :: RepositoryId -> Actor ()
analyse rid = do
  repository <- actorDB $ get404 rid
  dir <- repoCloneDir repository
  pool <- asks actorEnvConnectionPool
  ref <- newIORef HS.empty
  now <- liftIO getCurrentTime
  let url = repositoryUrl repository
  logDebugNS "Actor: Analyse" $ "analysing " <> url
  aid <-
    actorDB $
    insert
      RepositoryAnalysis
        { repositoryAnalysisRepo = rid
        , repositoryAnalysisTimestamp = now
        , repositoryAnalysisStatus = Ongoing
        }
  logFunc <- askLoggerIO
  let aenv =
        AnalysisEnv
          { analysisEnvRepositoryId = rid
          , analysisEnvAnalysisId = aid
          , analysisEnvConnectionPool = pool
          , analysisEnvObjsAnalysed = ref
          }
  liftIO $ runLoggingT (runReaderT (unAnalysisM (doAnalysis dir)) aenv) logFunc
  actorDB $ update aid [RepositoryAnalysisStatus =. Done]
