module Snotify.Actor.Monad where

import Snotify.Handler.Import

import Database.Persist.Sql (ConnectionPool, SqlPersistT, runSqlPool)
import Network.HTTP.Client as Http

import Snotify.Actor.Queue

import Snotify.Utils.Git

data ActorEnv =
  ActorEnv
    { actorEnvApp :: App
    , actorEnvConnectionPool :: ConnectionPool
    , actorEnvActionQueue :: ActionQueue
    , actorEnvCacheDir :: Path Abs Dir
    , actorEnvGitExecutable :: Maybe FilePath
    , actorEnvHttpManager :: Http.Manager
    }

instance HasCacheDir ActorEnv where
  askCacheDir = actorEnvCacheDir

type Actor = ReaderT ActorEnv (LoggingT IO)

runActor :: ActorEnv -> Actor a -> LoggingT IO a
runActor = flip runReaderT

actorDB :: SqlPersistT Actor a -> Actor a
actorDB action = do
  pool <- asks actorEnvConnectionPool
  runSqlPool action pool

runActorGit :: [String] -> Actor ()
runActorGit args = do
  mge <- asks actorEnvGitExecutable
  liftIO $ runGitRawIn mge Nothing args

runActorGitIn :: Path Abs Dir -> [String] -> Actor ()
runActorGitIn dir args = do
  mge <- asks actorEnvGitExecutable
  liftIO $ runGitRawIn mge (Just $ fromAbsDir dir) args
