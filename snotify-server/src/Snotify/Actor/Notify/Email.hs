{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Actor.Notify.Email
  ( Email(..)
  , sendSingleEmail
  ) where

import Snotify.Actor.Import

import qualified Data.Text as T

import Network.HTTP.Types as Http

import Control.Lens

import qualified Data.ByteString.Builder as SBB

import qualified Control.Monad.Logger as ML
import qualified Control.Monad.Trans.AWS as AWS

import Control.Monad.Trans.AWS hiding (seconds)
import Network.AWS.SES

data Email =
  Email
    { emailTo :: Text
    , emailFromName :: Text
    , emailFrom :: Text
    , emailSubject :: Text
    , emailTextContent :: Text
    , emailHtmlContent :: Text
    }
  deriving (Show, Eq)

sendSingleEmail :: Email -> Actor (Either Text Text)
sendSingleEmail Email {..} = do
  logFunc <- askLoggerIO
  let lgr :: Logger
      lgr ll b =
        logFunc defaultLoc "EMAILER" (modLogLevel ll) $ ML.toLogStr $ SBB.toLazyByteString b
      modLogLevel :: AWS.LogLevel -> ML.LogLevel
      modLogLevel ll =
        case ll of
          AWS.Info -> ML.LevelInfo
          AWS.Error -> ML.LevelError
          AWS.Debug -> ML.LevelDebug
          AWS.Trace -> ML.LevelDebug
  env <- set envLogger lgr <$> newEnv Discover
  runResourceT . runAWST env $ do
    let txt = content emailTextContent
    let html = content emailHtmlContent
    let bod = body & bText .~ Just txt & bHTML .~ Just html
    let sub = content emailSubject
    let mesg = message sub bod
    let dest = destination & dToAddresses .~ [emailTo]
    let req = sendEmail emailFrom dest mesg
    errOrResp <- trying _ServiceError (send req)
    pure $
      case errOrResp of
        Right resp ->
          case resp ^. sersResponseStatus of
            200 -> Right $ resp ^. sersMessageId
            _ -> Left "Unknown error while sending email."
        Left ServiceError' {..} ->
          let stat = _serviceStatus
           in Left $
              T.unwords
                [ "Service error:"
                , T.pack $ show $ Http.statusCode stat
                , T.pack $ show $ Http.statusMessage stat
                , T.pack (show _serviceCode)
                , maybe "" (T.pack .show)_serviceMessage
                ]
