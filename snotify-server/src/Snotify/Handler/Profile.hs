{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Handler.Profile
  ( getProfileR
  , postProfileGithubImportR
  ) where

import Snotify.Handler.Import

getProfileR :: Handler Html
getProfileR = do
  uid <- requireAuthId
  mgu <- runDB $ selectFirst [GithubUserUser ==. uid] []
  defaultLayout $ do
    setTitle "Profile"
    $(widgetFile "profile")

postProfileGithubImportR :: Handler Html
postProfileGithubImportR = do
  uid <- requireAuthId
  mgu <- runDB $ selectFirst [GithubUserUser ==. uid] []
  case mgu of
    Nothing -> invalidArgs ["No github account associated with this user."]
    Just (Entity guid _) -> do
      scheduleAction $ ActionGithubImport guid
      setMessage "Github Import Scheduled."
      redirect ProfileR
