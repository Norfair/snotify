{-# LANGUAGE TemplateHaskell #-}

module Snotify.Handler.Docs
  ( getDocsR
  ) where

import Snotify.Handler.Import

getDocsR :: Handler Html
getDocsR = defaultLayout $(widgetFile "docs")
