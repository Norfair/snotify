{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Handler.Home
    ( getHomeR
    ) where

import Snotify.Handler.Import

getHomeR :: Handler Html
getHomeR = defaultLayout $(widgetFile "home")
