{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Handler.Admin.Users
  ( getAdminUsersR
  ) where

import Snotify.Handler.Import

getAdminUsersR :: Handler Html
getAdminUsersR = do
  users <-
    runDB $ do
      users <- selectList [] [Asc UserId]
      dummyUsers <- map entityVal <$> selectList [] [Asc DummyUserId]
      githubUsers <- map entityVal <$> selectList [] [Asc GithubUserId]
      adminUsers <- map entityVal <$> selectList [] [Asc AdminUserId]
      pure $
        flip mapMaybe users $ \e@(Entity uid user) ->
          (,) e <$>
          case userAuthMethod user of
            ByDummy -> dummyUserIdent <$>   find ((== uid) . dummyUserUser) dummyUsers
            ByGithub -> githubUserIdent <$> find ((== uid) . githubUserUser) githubUsers
            ByAdmin -> adminUserIdent <$>   find ((== uid) . adminUserUser) adminUsers
  defaultLayout $(widgetFile "admin/users")
