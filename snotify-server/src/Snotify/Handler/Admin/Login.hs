{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Handler.Admin.Login
  ( getAdminLoginR
  , postAdminLoginR
  ) where

import Snotify.Handler.Import

import qualified Yesod.Auth.Email as YAE

import Yesod.Form.Bootstrap4 (BootstrapFormLayout(..), bfs, renderBootstrap4)

data Login =
  Login
    { loginUsername :: Text
    , loginPassword :: Text
    }
  deriving (Show, Eq)

loginForm :: Form Login
loginForm =
  renderBootstrap4 BootstrapBasicForm $
  Login <$> areq textField (bfs ("username" :: Text)) Nothing <*>
  areq passwordField (bfs ("password" :: Text)) Nothing

getAdminLoginR :: Handler Html
getAdminLoginR = do
  (formWidget, formEnctype) <- generateFormPost loginForm
  defaultLayout $(widgetFile "admin/login")

postAdminLoginR :: Handler TypedContent
postAdminLoginR = do
  ((result, formWidget), formEnctype) <- runFormPost loginForm
  case result of
    FormSuccess Login {..} -> do
      mau <- runDB $ getBy $ UniqueAdmin loginUsername
      let deny = invalidArgs ["Not Authorised"]
      case mau of
        Nothing -> deny
        Just (Entity _ AdminUser {..}) -> do
          if YAE.isValidPass loginPassword adminUserPass
            then do
                setCreds False
                   Creds {credsPlugin = "admin", credsIdent = adminUserIdent, credsExtra = []}
                redirect $ AdminR AdminPanelR
            else deny
    _ -> selectRep $ provideRep $ defaultLayout $(widgetFile "admin/login")
