{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Handler.Admin.User
  ( getAdminUserR
  ) where

import Snotify.Handler.Import

import Snotify.Utils.Repository

getAdminUserR :: UserId -> Handler Html
getAdminUserR uid = do
  user <- runDB $ get404 uid
  repoCount <- runDB $ count ([RepositoryUser ==. uid] :: [Filter Repository])
  activeRepoCount <-
    runDB $ count ([RepositoryUser ==. uid, RepositoryEnabled ==. True] :: [Filter Repository])
  rs <- runDB $ selectList [RepositoryUser ==. uid, RepositoryEnabled ==. True] []
  emailAddresses <-
    fmap (map entityVal) $
    runDB $
    selectList
      [UserEmailAddressUser ==. uid]
      [Desc UserEmailAddressPrimary, Desc UserEmailAddressVerified]
  repos <- fmap (sortOn snd) $ forM rs $ \e@(Entity _ r) -> (,) e <$> repositorySpaceUsage r
  defaultLayout $(widgetFile "admin/user")
