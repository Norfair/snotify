{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Handler.Admin.Repo
  ( getAdminRepoR
  ) where

import Snotify.Handler.Import

import Snotify.Utils.Repository
import Snotify.Utils.Secret

getAdminRepoR :: RepositoryId -> Handler Html
getAdminRepoR rid = do
  repo <- runDB $ get404 rid
  analyses <- runDB $ selectList [RepositoryAnalysisRepo ==. rid] []
  commitsAnalysed <- runDB $ count [CommitAnalysedRepo ==. rid]
  alreadyCloned <- repoCloned repo
  b@(Bytes su) <- repositorySpaceUsage repo
  keysFound <- runDB $ selectList [SecretFoundRepo ==. rid] []
  defaultLayout $(widgetFile "admin/repo")
