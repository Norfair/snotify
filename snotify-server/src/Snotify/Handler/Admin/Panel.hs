{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Handler.Admin.Panel
  ( getAdminPanelR
  , postAdminBulkImportGithubR
  ) where

import Snotify.Handler.Import

getAdminPanelR :: Handler Html
getAdminPanelR = defaultLayout $(widgetFile "admin/panel")

postAdminBulkImportGithubR :: Handler Html
postAdminBulkImportGithubR = do
  gus <- runDB $ selectList [] []
  mapM_ (scheduleAction . ActionGithubImport .entityKey) gus
  redirect $ AdminR AdminPanelR
