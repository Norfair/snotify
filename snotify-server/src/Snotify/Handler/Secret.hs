module Snotify.Handler.Secret
  ( postSecretSolvedR
  , postSecretFalsePositiveR
  ) where

import Snotify.Handler.Import

postSecretSolvedR :: SecretFoundId -> Handler Html
postSecretSolvedR sid = do
  runDB $ update sid [SecretFoundStatus =. Just Solved]
  redirect OverviewR

postSecretFalsePositiveR :: SecretFoundId -> Handler Html
postSecretFalsePositiveR sid = do
  runDB $ update sid [SecretFoundStatus =. Just FalsePositive]
  redirect OverviewR
