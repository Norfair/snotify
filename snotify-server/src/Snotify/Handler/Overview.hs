{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Handler.Overview
  ( getOverviewR
  , postOverviewR
  ) where

import Snotify.Handler.Import

import Snotify.Utils.Secret

import Yesod.Form.Bootstrap4 (BootstrapFormLayout(..), renderBootstrap4)

newtype RegisterRepository =
  RegisterRepository
    { registerRepositoryUrl :: Text
    }
  deriving (Show, Eq)

getOverviewR :: Handler Html
getOverviewR = do
  uid <- requireAuthId
  (formWidget, formEnctype) <- generateFormPost repositoryForm
  repos <- runDB $ selectList [RepositoryUser ==. uid] [Desc RepositoryEnabled]
  keysFound <-
    runDB $ selectList [SecretFoundRepo <-. map entityKey repos, SecretFoundStatus ==. Nothing] []
  let tups =
        flip mapMaybe keysFound $ \ek@(Entity _ keyFound) ->
          (,) <$> find ((== (secretFoundRepo keyFound)) . entityKey) repos <*> pure ek
  defaultLayout $(widgetFile "overview")

repositoryForm :: Form RegisterRepository
repositoryForm =
  renderBootstrap4 BootstrapInlineForm $ RegisterRepository <$> areq textField textSettings Nothing
  where
    textSettings =
      FieldSettings
        { fsLabel = "URL"
        , fsTooltip = Nothing
        , fsId = Nothing
        , fsName = Nothing
        , fsAttrs = [("class", "form-control"), ("placeholder", "URL")]
        }

postOverviewR :: Handler Html
postOverviewR = do
  uid <- requireAuthId
  ((result, _), _) <- runFormPost repositoryForm
  case result of
    FormSuccess RegisterRepository {..} -> do
      runDB $
        insert_
          Repository
            {repositoryEnabled = True, repositoryUser = uid, repositoryUrl = registerRepositoryUrl}
      redirect OverviewR
    _ -> redirect OverviewR -- TODO do something with the form result
