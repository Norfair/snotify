{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}

module Snotify.Handler.Repository
  ( getRepositoryR
  , postRepositoryEnableR
  , postRepositoryDisableR
  , postRepositoryCloneR
  , postRepositoryFetchR
  , postRepositoryAnalyseR
  ) where

import Snotify.Handler.Import

import Snotify.Utils.Secret

getRepositoryR :: RepositoryId -> Handler Html
getRepositoryR rid = do
  void requireAuthId
  repo <- runDB $ get404 rid
  mLatestAnalysis <-
    runDB $
    selectFirst
      [RepositoryAnalysisRepo ==. rid, RepositoryAnalysisStatus ==. Done]
      [Desc RepositoryAnalysisTimestamp]
  mOngoingAnalysis <-
    runDB $
    selectFirst
      [RepositoryAnalysisRepo ==. rid, RepositoryAnalysisStatus !=. Done]
      [Desc RepositoryAnalysisTimestamp]
  keysFound <- runDB $ selectList [SecretFoundRepo ==. rid] [Asc SecretFoundStatus]
  defaultLayout $(widgetFile "repository")

postRepositoryEnableR :: RepositoryId -> Handler Html
postRepositoryEnableR rid = do
  runDB $ update rid [RepositoryEnabled =. True]
  redirect OverviewR

postRepositoryDisableR :: RepositoryId -> Handler Html
postRepositoryDisableR rid = do
  runDB $ update rid [RepositoryEnabled =. False]
  redirect OverviewR

postRepositoryCloneR :: RepositoryId -> Handler Html
postRepositoryCloneR rid = do
  scheduleAction $ ActionClone rid
  setMessage "Repository cloning is scheduled."
  redirect $ RepositoryR rid

postRepositoryFetchR :: RepositoryId -> Handler Html
postRepositoryFetchR rid = do
  scheduleAction $ ActionFetch rid
  setMessage "Repository fetching is scheduled."
  redirect $ RepositoryR rid

postRepositoryAnalyseR :: RepositoryId -> Handler Html
postRepositoryAnalyseR rid = do
  scheduleAction $ ActionEnsureAnalyse rid
  setMessage "Repository analysis is scheduled. Come back when it's done."
  redirect $ RepositoryR rid
