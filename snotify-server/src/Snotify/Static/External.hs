{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Snotify.Static.External where


import Snotify.Import

import qualified Data.ByteString as SB
import Language.Haskell.TH
import Network.Curl.Download

ensureExternalStatic :: Q [Dec]
ensureExternalStatic = do
  runIO $ do
    ensureFile
      [relfile|static/css/bootstrap.css|]
      "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    ensureFile
      [relfile|static/js/bootstrap.js|]
      "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    ensureFile
      [relfile|static/css/fontawesome.css|]
      "https://use.fontawesome.com/releases/v5.8.1/css/all.css"
  pure []

ensureFile :: Path Rel File -> String -> IO ()
ensureFile rp url = do
  mExists <- forgivingAbsence $ doesFileExist rp
  unless (mExists == Just True) $ do
    putStrLn $ unwords ["Downloading", url, "to put it at", fromRelFile rp]
    errOrBs <- openURI url
    case errOrBs of
      Left err -> fail $ unwords ["Failed to download ", url, "with error", err]
      Right bs -> do
        ensureDir (parent rp)
        SB.writeFile (fromRelFile rp) bs
