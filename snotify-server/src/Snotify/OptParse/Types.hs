{-# LANGUAGE DeriveGeneric #-}

module Snotify.OptParse.Types where

import Snotify.Import

data Instructions =
  Instructions Dispatch Settings
  deriving (Show, Eq, Generic)

data Dispatch =
  DispatchServe ServeSettings
  deriving (Show, Eq, Generic)

data ServeSettings =
  ServeSettings
    { serveSetDbFile :: Path Abs File
    , serveSetLogLevel :: LogLevel
    , serveSetHost :: Maybe Text
    , serveSetPort :: Int
    , serveSetCacheDir :: Path Abs Dir
    , serveSetActorQueueSize :: Natural
    , serveSetTracking :: Maybe Text
    , serveSetVerification :: Maybe Text
    , serveSetGithubSettings :: Maybe GithubSettings
    , serveSetGitExecutable :: Maybe FilePath
    , serveSetLoopersSettings :: LoopersSettings
    }
  deriving (Show, Eq, Generic)

data GithubSettings =
  GithubSettings
    { githubSettingsClientId :: Text
    , githubSettingsClientSecret :: Text
    }
  deriving (Show, Eq, Generic)

data LoopersSettings =
  LoopersSettings
    { loopersSetAnalysisSettings :: LooperSettings
    , loopersSetEmailerSettings :: LooperSettings
    }
  deriving (Show, Eq, Generic)

data LooperSettings =
  LooperSettings
    { looperSetEnabled :: Bool
    , looperSetPhase :: NominalDiffTime
    , looperSetPeriod :: NominalDiffTime
    }
  deriving (Show, Eq, Generic)

data Settings =
  Settings
  deriving (Show, Eq, Generic)

data Arguments =
  Arguments Command Flags

data Command =
  CommandServe ServeFlags

data ServeFlags =
  ServeFlags
    { serveFlagDbFile :: Maybe FilePath
    , serveFlagLogLevel :: Maybe LogLevel
    , serveFlagHost :: Maybe Text
    , serveFlagPort :: Maybe Int
    , serveFlagCacheDir :: Maybe FilePath
    , serveFlagActorQueueSize :: Maybe Natural
    , serveFlagTracking :: Maybe Text
    , serveFlagVerification :: Maybe Text
    , serveFlagGithubSettings :: Maybe GithubSettings
    , serveFlagGitExecutable :: Maybe FilePath
    , serveFlagLoopersFlags :: LoopersFlags
    }
  deriving (Show, Eq, Generic)

data LoopersFlags =
  LoopersFlags
    { loopersFlagAnalysisFlags :: LooperFlags
    , loopersFlagEmailerFlags :: LooperFlags
    }
  deriving (Show, Eq, Generic)

data LooperFlags =
  LooperFlags
    { looperFlagEnabled :: Maybe Bool
    , looperFlagPhase :: Maybe Int -- Seconds
    , looperFlagPeriod :: Maybe Int -- Seconds
    }
  deriving (Show, Eq, Generic)

data Flags =
  Flags
  deriving (Show, Eq, Generic)

data Environment =
  Environment
    { envDBFile :: Maybe FilePath
    , envLogLevel :: Maybe LogLevel
    , envPort :: Maybe Int
    , envHost :: Maybe Text
    , envCacheDir :: Maybe FilePath
    , envActorQueueSize :: Maybe Natural
    , envTracking :: Maybe Text
    , envVerification :: Maybe Text
    , envGithubSettings :: Maybe GithubSettings
    , envGitExecutable :: Maybe FilePath
    , envLoopersEnvironment :: LoopersEnvironment
    }
  deriving (Show, Eq, Generic)

data LoopersEnvironment =
  LoopersEnvironment
    { loopersEnvAnalysisEnvironment :: LooperEnvironment
    , loopersEnvEmailerEnvironment :: LooperEnvironment
    }
  deriving (Show, Eq, Generic)

data LooperEnvironment =
  LooperEnvironment
    { looperEnvEnabled :: Maybe Bool
    , looperEnvPhase :: Maybe Int -- Seconds
    , looperEnvPeriod :: Maybe Int -- Seconds
    }
  deriving (Show, Eq, Generic)
