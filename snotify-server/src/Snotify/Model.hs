{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Snotify.Model
  ( module Snotify.Model
  , module Snotify.Model.AuthMethod
  , module Snotify.Model.DB
  , module Snotify.Model.Fixtures
  , module Snotify.Model.SecretStatus
  , module Snotify.Model.SecretType
  , module Snotify.Model.SecretNotificationEmailStatus
  , module Snotify.Model.RepositoryAnalysisStatus
  ) where

import Snotify.Import

import Database.Persist.Sql

import Snotify.Model.AuthMethod
import Snotify.Model.DB
import Snotify.Model.Fixtures
import Snotify.Model.RepositoryAnalysisStatus
import Snotify.Model.SecretNotificationEmailStatus
import Snotify.Model.SecretType
import Snotify.Model.SecretStatus

setupDatabase :: MonadIO m => ReaderT SqlBackend m ()
setupDatabase = do
  runMigration migrateAll
  runFixtures
