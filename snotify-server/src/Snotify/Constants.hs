{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Snotify.Constants where

import Snotify.Import

import Yesod.Core.Types

development :: Bool
#ifdef DEVELOPMENT
development = True
#else
development = False
#endif
resolvedRoot :: ResolvedApproot -- TODO pass these in from settings instead of via 'development'
resolvedRoot =
  if development
    then "http://localhost"
    else "https://snotify.cs-syd.eu"
