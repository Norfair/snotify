{-# LANGUAGE CPP #-}

module Snotify.Import.NoFoundation
  ( module Import
  , widgetFile
  ) where

import Database.Persist as Import
import Database.Persist.Sql as Import
import Database.Persist.Sqlite as Import

import Snotify.Constants as Import
import Snotify.Import as Import
import Snotify.Model as Import
import Snotify.OptParse.Types as Import
import Snotify.Static as Import

import Yesod as Import hiding (parseTime)
import Yesod.Auth as Import
import Yesod.Core.Types as Import (loggerSet)
import Yesod.Default.Config2 as Import
import Yesod.Default.Util (widgetFileNoReload, widgetFileReload)

import Language.Haskell.TH

-- The rest of this file contains settings which rarely need changing by a
-- user.
widgetFile :: String -> Q Exp
widgetFile =
  (if development
     then widgetFileReload
     else widgetFileNoReload)
    def
