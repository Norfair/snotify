{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Snotify.Static where

import Snotify.Import

import Yesod.EmbeddedStatic

import Snotify.Constants
import Snotify.Static.External

ensureExternalStatic

-- This generates easy references to files in the static directory at compile time,
-- giving you compile-time verification that referenced files exist.
-- Warning: any files added to your static directory during run-time can't be
-- accessed this way. You'll have to use their FilePath or URL to access them.
--
-- For example, to refer to @static/js/script.js@ via an identifier, you'd use:
--
--     js_script_js
--
-- If the identifier is not available, you may use:
--
--     StaticFile ["js", "script.js"] []
mkEmbeddedStatic
  development
  "snotifyStatic"
  (let embedStatic f = embedFileAt f ("static/" ++ f)
    in [ embedStatic "css/bootstrap.css"
       , embedStatic "js/bootstrap.js"
       , embedStatic "css/fontawesome.css"
       , embedStatic "ssh.png"
       , embedStatic "stripe.png"
       ])
