{-# LANGUAGE OverloadedStrings #-}

module Snotify.Actor.AnalyseSpec where

import TestImport

import Snotify.Actor.Analyse
import Snotify.Actor.Clone
import Snotify.Actor.EnsureAnalyse
import Snotify.Actor.Fetch
import Snotify.Actor.Monad

spec :: Spec
spec =
  actorSpec $ do
    describe "clone" $
      it "can successfully clone the repo with a secret" $ \ae ->
        runTestActor ae $ do
          rid <- setupTestRepoInDb
          clone rid
    describe "fetch" $
      it "can successfully fetch the repo with a secret" $ \ae ->
        runTestActor ae $ do
          rid <- setupTestRepoInDb
          clone rid
          fetch rid
    describe "analyse" $
      it "can successfully analyse the repo with a secret" $ \ae ->
        runTestActor ae $ do
          rid <- setupTestRepoInDb
          clone rid
          analyse rid
          ensureCorrectSecretsFound rid
    describe "ensureAnalyse" $
      it "can successfully analyse the repo with a secret" $ \ae ->
        runTestActor ae $ do
          rid <- setupTestRepoInDb
          ensureAnalyse rid
          ensureCorrectSecretsFound rid

setupTestRepoInDb :: Actor RepositoryId
setupTestRepoInDb =
  actorDB $ do
    uid <- createDummyUser "testuser"
    insert
      Repository
        { repositoryUser = uid
        , repositoryUrl = "https://github.com/NorfairKing/repo-with-secret.git"
        , repositoryEnabled = True
        }

ensureCorrectSecretsFound :: RepositoryId -> Actor ()
ensureCorrectSecretsFound rid = do
  keysFound <- actorDB $ selectList [SecretFoundRepo ==. rid] []
  liftIO $
    map (secretFoundCommit . entityVal) keysFound `shouldBe`
    ["db3d11d89ca3a97b6420d1a72edc5515e4f4319c"]
