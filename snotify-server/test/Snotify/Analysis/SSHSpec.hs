{-# LANGUAGE OverloadedStrings #-}

module Snotify.Analysis.SSHSpec where

import TestImport

import Snotify.Analysis.SSH

spec :: Spec
spec =
  describe "findSSHKey" $ do
    it "finds an RSA key" $
      forAllValid $ \(b, key, a) -> do
        let s =
              b <> "-----BEGIN RSA PRIVATE KEY-----" <> key <> "-----END RSA PRIVATE KEY-----" <>
              a
        findSSHKey s `shouldBe` Just (normaliseSSHKey key)
    it "finds an OPENSSH key" $
      forAllValid $ \(b, key, a) -> do
        let s =
              b <> "-----BEGIN OPENSSH PRIVATE KEY-----" <> key <>
              "-----END OPENSSH PRIVATE KEY-----" <>
              a
        findSSHKey s `shouldBe` Just (normaliseSSHKey key)
