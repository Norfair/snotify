{-# LANGUAGE OverloadedStrings #-}

module Snotify.Analysis.StripeSpec where

import TestImport

import qualified Data.ByteString.Char8 as SB8

import Snotify.Analysis.Stripe

spec :: Spec
spec =
  describe "findSSHKey" $ do
    it "finds a secret test key" $
      forAllValid $ \(b, a) -> do
        forAll (replicateM 24 genValid) $ \chars -> do
          let key = SB8.pack chars
          let s = b <> "sk_test_" <> key <> a
          findStripeKey s `shouldBe` Just ("sk_test_" <> key)
    it "finds a secret live key" $
      forAllValid $ \(b, a) ->
        forAll (replicateM 24 genValid) $ \chars -> do
          let key = SB8.pack chars
          let s = b <> "sk_live_" <> key <> a
          findStripeKey s `shouldBe` Just ("sk_live_" <> key)
