module Main where

import TestImport

import qualified Handler.CommonSpec
import qualified Handler.HomeSpec
import qualified Handler.ProfileSpec

import qualified Snotify.Actor.AnalyseSpec
import qualified Snotify.Analysis.SSHSpec
import qualified Snotify.Analysis.StripeSpec

main :: IO ()
main = do
  hSetBuffering stdout LineBuffering
  let extra = do
        Snotify.Actor.AnalyseSpec.spec
        Snotify.Analysis.SSHSpec.spec
        Snotify.Analysis.StripeSpec.spec
  snotifySpec extra $ do
    Handler.CommonSpec.spec
    Handler.HomeSpec.spec
    Handler.ProfileSpec.spec
