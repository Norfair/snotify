{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Handler.ProfileSpec
  ( spec
  ) where

import TestImport

spec :: YesodSpec App
spec =
  ydescribe "Profile page" $ do
    yit "asserts no access to my-account for anonymous users" $ do
      get ProfileR
      statusIs 403
    yit "asserts access to my-account for authenticated users" $ do
      let ident = "foo"
      void $ createUser ident
      authenticateAsDummy ident
      get ProfileR
      statusIs 200
