module Handler.CommonSpec
  ( spec
  ) where

import TestImport

spec :: YesodSpec App
spec =
  ydescribe "robots.txt" $ do
    yit "gives a 200" $ do
      get RobotsR
      statusIs 200
    yit "has correct User-agent" $ do
      get RobotsR
      bodyContains "User-agent: *"
