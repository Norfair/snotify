{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module TestImport
  ( module TestImport
  , module X
  ) where

import Snotify.Import as X

import Data.GenValidity as X
import Data.GenValidity.ByteString as X
import qualified Data.Text as T
import Database.Persist as X hiding (get)
import Database.Persist.Sql
  ( SqlPersistM
  , connEscapeName
  , rawExecute
  , rawSql
  , runSqlPersistMPool
  , unSingle
  )
import qualified Database.Persist.Sqlite as DB
import Lens.Micro
import qualified Network.HTTP.Client as Http
import qualified Network.HTTP.Client.TLS as Http
import Snotify.Application
import Snotify.Foundation as X
import Snotify.Model as X
import Snotify.Static as X
import Test.Hspec as X
import Test.QuickCheck as X
import Test.Validity as X
import Yesod.Auth as X
import Yesod.Test as X

import Snotify.Actor.Monad
import Snotify.Actor.Queue

actorSpec :: SpecWith ActorEnv -> Spec
actorSpec = around go
  where
    go :: (ActorEnv -> IO ()) -> IO ()
    go func = do
      cacheDir <- resolveDir' "cache"
      -- withSystemTempDir "snotify-cache" $ \cacheDir ->
      withSystemTempFile "snotify-test" $ \dbFile _ ->
          runNoLoggingT $
          DB.withSqlitePoolInfo
            (DB.mkSqliteConnectionInfo (T.pack $ toFilePath dbFile) & DB.walEnabled .~ False &
             DB.fkEnabled .~ False)
            1 $ \pool ->
            liftIO $ do
              actionQueue <- newActionQueue 100
              void $ DB.runSqlPool (DB.runMigrationSilent migrateAll) pool
              func
                ActorEnv
                  { actorEnvConnectionPool = pool
                  , actorEnvCacheDir = cacheDir
                  , actorEnvActionQueue = actionQueue
                  , actorEnvGitExecutable = Nothing
                  }

runTestActor :: ActorEnv -> Actor a -> IO a
runTestActor ae = runStderrLoggingT . runActor ae

snotifySpec :: Spec -> YesodSpec App -> IO ()
snotifySpec extraSpecs spec = do
  httpManager <- Http.newManager Http.tlsManagerSettings
  withSystemTempDir "snotify-cache" $ \cacheDir ->
    withSystemTempFile "snotify-test" $ \dbFile _ ->
      runNoLoggingT $
      DB.withSqlitePoolInfo
        (DB.mkSqliteConnectionInfo (T.pack $ toFilePath dbFile) & DB.walEnabled .~ False &
         DB.fkEnabled .~ False)
        1 $ \pool ->
        liftIO $ do
          void $ DB.runSqlPool (DB.runMigrationSilent migrateAll) pool
          actionQueue <- newActionQueue 100
          let go = do
                let app =
                      App
                        { appRoot = Nothing
                        , appStatic = snotifyStatic
                        , appConnPool = pool
                        , appHttpManager = httpManager
                        , appDummyAuth = True
                        , appGoogleAnalyticsTracking = Nothing
                        , appGoogleSearchConsoleVerification = Nothing
                        , appGithubSettings = Nothing
                        , appGitExecutable = Nothing
                        , appCacheDir = cacheDir
                        , appActionQueue = actionQueue
                        }
                wipeDB app
                pure app
          hspec $ do
            extraSpecs
            yesodSpecWithSiteGenerator go spec

runDB :: SqlPersistM a -> YesodExample App a
runDB query = do
  pool <- fmap appConnPool getTestYesod
  liftIO $ runSqlPersistMPool query pool

-- This function will truncate all of the tables in your database.
-- 'withApp' calls it before each test, creating a clean environment for each
-- spec to run in.
wipeDB :: App -> IO ()
wipeDB app = do
  let pool = appConnPool app
  flip DB.runSqlPool pool $ do
    tables <- getTables
    sqlBackend <- ask
    let queries = map (\t -> "DELETE FROM " <> (connEscapeName sqlBackend $ DBName t)) tables
    forM_ queries (\q -> rawExecute q [])

getTables :: DB [Text]
getTables = do
  tables <- rawSql "SELECT name FROM sqlite_master WHERE type = 'table';" []
  return (fmap unSingle tables)

-- | Authenticate as a user. This relies on the `auth-dummy-login: true` flag
-- being set in test-settings.yaml, which enables dummy authentication in
-- Foundation.hs
authenticateAsDummy :: Text -> YesodExample App ()
authenticateAsDummy ident = do
  request $ do
    setMethod "POST"
    addPostParam "ident" ident
    setUrl $ AuthR $ PluginR "dummy" []

-- | Create a user.  The dummy email entry helps to confirm that foreign-key
-- checking is switched off in wipeDB for those database backends which need it.
createUser :: Text -> YesodExample App UserId
createUser ident = runDB $ createDummyUser ident
