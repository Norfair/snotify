#!/bin/bash

set -e
set -x

export SNOTIFY_LOG_LEVEL=LevelDebug
export SNOTIFY_LOOPER_ANALYSIS_ENABLED=False
# export SNOTIFY_LOOPER_EMAILER_ENABLED=False
export SNOTIFY_LOOPER_EMAILER_PHASE=0

stack install :snotify-server --file-watch --exec="./scripts/redo.sh $@" --ghc-options="-freverse-errors -DDEVELOPMENT -O0" --fast --pedantic --flag snotify-server:dev
