#!/bin/bash

set -e
set -x

cd snotify-server

killall snotify-server || true

snotify-server serve $@ &
