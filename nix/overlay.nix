final:
  previous:
    with final.haskell.lib;
    {
      snotifyPackages = let
        pathFor = name:
          builtins.path {
            inherit name;
            path = ../. + "/${name}";
            filter = path:
              type:
                !final.lib.hasPrefix "." (baseNameOf path);
          };
        snotifyPkg = name:
          dontHaddock (failOnAllWarnings (disableLibraryProfiling
              (addBuildDepends (final.haskellPackages.callCabal2nix name (pathFor name) {}) [final.git final.cacert])));
      in final.lib.genAttrs [
        "snotify-server"
      ] snotifyPkg;
      haskellPackages = previous.haskellPackages.override (old:
        {
          overrides = final.lib.composeExtensions (old.overrides or (_:
            _:
              {})) (self:
            super:
              final.snotifyPackages // {
                yesod-auth-oauth2 = final.haskellPackages.callCabal2nix "yesod-auth-oauth2" (final.fetchFromGitHub {
                  owner = "thoughtbot";
                  repo = "yesod-auth-oauth2";
                  rev = "4f1de3eb85c458729ff509aa95b3dc11b1607b78";
                  sha256 = "11r73icrixs0qphy22qpr4aax6f447yl5k0472khvdqkj87qjibz";
                }) {};
                gitlib = final.haskell.lib.dontCheck (final.haskellPackages.callHackage "gitlib" "3.1.2" {});
                gitlib-libgit2 = final.haskell.lib.dontCheck (final.haskellPackages.callHackage "gitlib-libgit2" "3.1.2.1" {});
                # gitlib-test = final.haskellPackages.callHackage "gitlib-test" "3.1.2" {};
              });
        });
    }
