# To update:
# nix-prefetch-url --unpack https://github.com/NixOS/nixpkgs/archive/07723f14bb132f6d65c74b953bef39cd80457273.tar.gz

{
  rev    = "be5594b412e9ba5e52838783e7cac7320625d4ae";
  sha256 = "05vdhjq1cnpjdfd0wb38csdx437wm7fl8jqdvknjv7f2a0mrwd9p";
}
